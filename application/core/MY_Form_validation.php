<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Form_validation extends CI_Controller {

	protected $CI;

	function __construct($config = array())
	{
    	parent::__construct($config);
    	if(!function_exists('valid_form')) {
		    function valid_form(){
				foreach ($this->_error_array() as $errors) {
				echo '
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  '.$errors.'.
					</div>';
				}
			}
	    }
	}

	// public function validate_captcha($str)
	// {
		
	// 	$recaptcha = new \ReCaptcha\ReCaptcha($this->config->item('re-privatekey'));
 //        $resp = $recaptcha->verify($str, $this->input->server('REMOTE_ADDR'));
	// 	if ($resp->isSuccess()) {
	//     	return TRUE;
	// 	} else {
	// 		$this->set_message('validate_captcha', $resp->getErrorCodes());
	// 	return false;
	// 	}

	// }

}

/* End of file MY_Form_validation.php */
/* Location: ./application/core/MY_Form_validation.php */