<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Brand extends CI_Model
{
    protected $table = 'brands';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function select_all()
    {
        $data = $this->db
                     ->select('id, name')
                     ->from($this->table)
                     ->get();
        if ($data->num_rows() > 0) {
            return $data->result();
        }
    }

    public function select_name($id)
    {
        $data = $this->db
                     ->select('name')
                     ->from($this->table)
                     ->where('id', $id)
                     ->get();
        if ($data->num_rows() > 0) {
            $hasil = $data->row_array();
            return $hasil['name'];
        }
    }
}
