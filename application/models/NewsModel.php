<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NewsModel extends CI_Model {

	protected $table = 'news';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function getNews($id)
	{
		$data = $this->db
				->select('*')
				->from($this->table)
				->where('id', $id)
				->limit(1);
				->get();
		if ($data->num_rows() > 0) {
			return $data->array_rows();
		} else {
			return false;
		}
	}

}

/* End of file NewsModel.php */
/* Location: ./application/models/NewsModel.php */