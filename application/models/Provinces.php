<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provinces extends CI_Model {

	protected $table = 'provinces';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();

	}

	public function select_all()
	{
		$data = $this->db
					 ->select('id, name')
					 ->from($this->table)
					 ->get();
		if ($data->num_rows() > 0) {
			return $data->result_array();
		}
	}

}

/* End of file Provinces.php */
/* Location: ./application/models/Provinces.php */