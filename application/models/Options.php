<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Options extends CI_Model
{
    protected $table = 'options';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('cache_expire');
        if (!is_cache_valid('Options', 3600)) {
            $this->db->cache_delete_all();
        }
    }

    public function get($field)
    {
        // $this->db->cache_on();
        $data = $this->db
                    ->select($field)
                    ->from($this->table)
                    ->where('id', 1)
                    ->limit(1)
                    ->get();
        // $this->db->cache_off();
        if ($data->num_rows() > 0) {
            return $data->row_array()[$field];
        }
    }
}

/* End of file Options.php */
/* Location: ./application/models/Options.php */
