<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Villages extends CI_Model {

	protected $table = 'villages';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function WhereI()
	{
		
	}

	public function districtsId($id, $foreach = false)
	{
		$data = $this->db
				     ->select('id, name')
				     ->from($this->table)
				     ->where('district_id', $id)
				     ->get();
		if ($data->num_rows() > 0) {
			if ($foreach==true) {
				return $data->result_array();
			} else {
				return $data->result();
			}
		}
	}

}

/* End of file Villages.php */
/* Location: ./application/models/Villages.php */