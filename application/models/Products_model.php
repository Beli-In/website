<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Products_model extends CI_Model
{
    protected $table = "products";

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function count()
    {
        return $this->db->count_all($this->table);
    }

    public function select_all($limit = false, $start = false)
    {
        if ($limit!==false && $start == false) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($limit, $start);
        }
        $this->db->order_by('created_at', 'desc');
        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function insert($data)
    {
        if ($this->db->insert($this->table, $data)==true) {
            $data['last_id'] = $this->db->insert_id();
            return $data;
        }
    }

    public function update($data, $id)
    {
        $this->db->where('id', $id);
        if ($this->db->update($this->table, $data)) {
            return $data;
        }
        return false;
    }

    public function searchbyName($name, $limit, $start)
    {
        $this->db->like('name', $name);
        $this->db->limit($limit, $start);
        $this->db->order_by('created_at', 'desc');
        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    public function countbyName($name)
    {
        $this->db->like('name', $name);
        return $this->db->count_all($this->table);
    }

    public function selectbyId($id)
    {
        $data = $this->db
                     ->select('*')
                     ->from($this->table)
                     ->where('id', $id)
                     ->get();
        if ($data->num_rows() > 0) {
            return $data->row();
        }
        return false;
    }

    public function deletebyId($id)
    {
        $this->db->where('id', $id);
        if ($this->db->delete($this->table)==true) {
            return true;
        }
        return false;
    }
}
