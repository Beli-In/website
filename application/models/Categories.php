<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Categories extends CI_Model
{
    protected $table = 'categories';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function select_all()
    {
        $data = $this->db
                     ->select('id, name')
                     ->from($this->table)
                     ->get();
        if ($data->num_rows() > 0) {
            return $data->result();
        }
    }
}
