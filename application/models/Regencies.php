<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Regencies extends CI_Model {

	protected $table = "regencies";

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function whereId($Id)
	{
		$data = $this->db
				     ->select('id', 'name')
				     ->from($this->table)
				     ->where('id', $Id)
				     ->get();
		if ($data->num_rows() > 0) {
			return $data->row_array();
		}
	}

	public function ProvinceId($id, $foreach = false)
	{
		$data = $this->db
				     ->select('id, name')
				     ->from($this->table)
				     ->where('province_id', $id)
				     ->get();
		if ($data->num_rows() > 0) {
			if ($foreach==true) {
				return $data->result_array();
			} else {
				return $data->result();
			}
		}
	}

}

/* End of file Regencies.php */
/* Location: ./application/models/Regencies.php */