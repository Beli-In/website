<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Productsimage extends CI_Model
{
    protected $table = 'product_images';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function selectByKey($key, $limit = false, $cari = false)
    {
        $this->db
            ->select('filename, path, size, extension')
            ->from($this->table)
            ->where('key', $key);
        if ($limit!==false) {
            $this->db->limit($limit);
        }
        $data = $this->db->get();
        if ($data->num_rows() > 0 && $limit==false) {
            return $data->result();
        } elseif ($limit!==false) {
            $hasil = $data->row_array();
            return $hasil[$cari];
        }
        return false;
    }

    public function insert($data)
    {
        if ($this->db->insert($this->table, $data)==true) {
            $data['last_id'] = $this->db->insert_id();
            return $data;
        }
        return false;
    }

    public function deleteByKey($key)
    {
        $data = $this->db
                     ->select('filename')
                     ->from($this->table)
                     ->where('key', $key)
                     ->get();
        if ($data->num_rows() > 0) {
            foreach ($data->result() as $image) {
                unlink(FCPATH . 'media/'.$image->filename);
                unlink(FCPATH . 'media/thumbs/'.$image->filename);
                $this->db->where('id', $image->id);
                $this->db->delete($this->table);
            }
        }
    }

    public function deleteByName($file)
    {
        $this->db->where('filename', $file);
        $this->db->delete($this->table);
    }
}
