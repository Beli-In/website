<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ProductsHaveCategories extends CI_Model
{
    protected $table = 'products_have_categories';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function select_products_list($id)
    {
        $data = $this->db
                     ->select('cat.name, cat.slug, GROUP_CONCAT(cat.name) as catag_name')
                     ->from($this->table.' procat')
                     ->join('categories cat', 'cat.id = procat.categories_id', 'inner')
                     ->join('products pro', 'pro.id = procat.products_id', 'inner')
                     ->where('procat.products_id', $id)
                     ->order_by('procat.categories_id', 'asc')
                     ->get();
        if ($data->num_rows() > 0) {
            $hasil = $data->row_array();
            return $hasil['catag_name'];
        }
    }

    public function select_categories($id)
    {
        $data = $this->db
                     ->select()
                     ->from($this->table.' procat')
                     ->join('categories cat', 'cat.categories_id = procat.categories_id', 'left')
                     ->join('products pro', 'pro.products_id = procat.products_id', 'left')
                     ->where('procat.categories_id', $id)
                     ->order_by('procat.products_id', 'asc')
                     ->get();
        if ($data->num_rows() > 0) {
            return $data->result();
        }
    }

    public function insertProducts($id, $category)
    {
        if (is_array($category)) {
            foreach ($category as $cat) {
                $data = [
                    'products_id' => $id,
                    'categories_id' => $cat
                ];
                if ($this->db->insert($this->table, $data)==false) {
                    return false;
                }
            }
        } else {
            $data = [
                'products_id' => $id,
                'categories_id' => 0
            ];
            if ($this->db->insert($this->table, $data)==false) {
                return false;
            }
        }

        return false;
    }

    public function updatedProducts($id, $category)
    {
        $this->db->where('products_id', $id)->delete($this->table);
        if (is_array($category)) {
            foreach ($category as $cat) {
                $data = [
                    'products_id' => $id,
                    'categories_id' => $cat
                ];
                if ($this->db->insert($this->table, $data)==false) {
                    return false;
                }
            }
        } else {
            $data = [
                'products_id' => $id,
                'categories_id' => 0
            ];
            if ($this->db->insert($this->table, $data)==false) {
                return false;
            }
        }

        return false;
    }
    /**
     * Matched Product and Category
     * @param  int $product_id  Product Id
     * @param  Iint $category_id Category_id
     * @return bool              if is true
     */
    public function matched($product_id, $category_id)
    {
        $data = $this->db
                     ->select('*')
                     ->from($this->table)
                     ->where('products_id', $product_id)
                     ->where('categories_id', $category_id)
                     ->limit(1)
                     ->get();
        if ($data->num_rows() > 0) {
            return true;
        }
        return false;
    }
}
