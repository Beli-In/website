<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Districts extends CI_Model {

	protected $table = 'districts';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function whereId($id)
	{
		
	}

	public function RegencyId($id, $foreach = false)
	{
		$data = $this->db
				     ->select('id, name')
				     ->from($this->table)
				     ->where('regency_id', $id)
				     ->get();
		if ($data->num_rows() > 0) {
			if ($foreach==true) {
				return $data->result_array();
			} else {
				return $data->result();
			}
		}
	}

}

/* End of file Districts.php */
/* Location: ./application/models/Districts.php */