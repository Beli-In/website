<?php

defined('BASEPATH') or exit('No direct script access allowed');

class PagesControllers extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function home()
    {
    }


    public function aboutus()
    {
        $html['title'] = 'About Us'
        ;

        return view('pages/aboutus', $html);
    }

    public function contactus()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            // print_r($_POST);
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'name', 'trim|required|min_length[10]');
            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
            $this->form_validation->set_rules('massage', 'massage', 'trim|required|min_length[20]|max_length[360]');
            $this->form_validation->set_rules('g-recaptcha-response', 'g-recaptcha-response', 'trim|required|callback_validate_captcha');
            if ($this->form_validation->run() == true) {
            } else {
                // code...
            }
        }
        $this->load->helper('form');
        $html['title'] = 'Contact Us';
        $html['maps_key'] = (!is_null($this->Options->get('map_key'))) ? $this->Options->get('map_key') : null;
        $html['header'] = "
        ";
        $html['footer'] = "
            <script src='https://maps.googleapis.com/maps/api/js?key=".$html['maps_key']."'></script>
            <script>
                function initialize() {
                  var mapOptions = {
                    zoom: 18,
                    scrollwheel: false,
                    center: new google.maps.LatLng(".$this->Options->get('coordinate').")
                  };

                  var map = new google.maps.Map(document.getElementById('hastech'),
                      mapOptions);

                  var marker = new google.maps.Marker({
                    position: map.getCenter(),
                    // animation:google.maps.Animation.BOUNCE,
                    icon: '".assets('img/map-marker.png')."',
                    map: map
                  });

                   var infowindow = new google.maps.InfoWindow({
                        content: '<strong>".$this->Options->get('name')."</strong><br/><p>".$this->Options->get('address')."</p>',
                            });
                    infowindow.open(map,marker);
                }

                google.maps.event.addDomListener(window, 'load', initialize);
            </script>
        ";
        return view('pages/contactus', $html);
    }

    public function cart()
    {
    }
}
/* End of file PagesControllers.php */
/* Location: ./application/controllers/PagesControllers.php */
