<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error404Controllers extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('pages/404');
	}

}

/* End of file Error404Controllers.php */
/* Location: ./application/controllers/Error404Controllers.php */