<?php
defined('BASEPATH') or exit('No direct script access allowed');

class News extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return view('pages/news/index');
    }
}

/* End of file News.php */
/* Location: ./application/controllers/News.php */
