<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Products extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Products_model', 'Productsimage']);
    }

    public function index($page = 0)
    {
        if ($this->input->get('search')) {
            # code...
        }
        if ($this->input->get('category')) {
            # code...
        }
        if ($this->input->get('brand')) {
            # code...
        }
        $page = (is_integer($page)==false) ? 0 : $page;
        // $price
        // $config = array();
        // $config['total_rows'] = $this->Products_model->countbyName($search);
        // $config['per_page']  = 10;
        // $config['page_query_string'] = true;
        // $config['num_links'] = 5;
        // $this->pagination->initialize($config);
        $page = (is_numeric($this->input->get('page'))) ? $this->input->get('page') : 0;
        // $html["products"] = $this->Products_model->searchByname($search, $config["per_page"], $page);
        // $html["pagination"] = $this->pagination->create_links();
        // $html['products'] = $this->Products_model->select_all(8);

        $html['header'] = "
			<link rel='stylesheet' href=".assets('css/animate.css').">
			<link href=".assets('css/jquery-ui.css')." rel='stylesheet'>
        ";

        $html['footer'] = "
	        <script src=".assets('js/jquery-price-slider.js')."></script>
		";
        return view('pages/products/index', $html);
    }

    public function show($slug)
    {
    }
}

/* End of file Products.php */
/* Location: ./application/controllers/Products.php */
