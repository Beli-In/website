<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class AccountControllers extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
	}

	public function index()
	{
		
	}

	public function validate_captcha($str)
	{
		
		$recaptcha = new \ReCaptcha\ReCaptcha($this->config->item('re-privatekey'));
        $resp = $recaptcha->verify($str, $this->input->server('REMOTE_ADDR'));
        $this->form_validation->set_message('validate_captcha', $resp->getErrorCodes());
		if ($resp->isSuccess()) {
	    	return TRUE;
		} else {
			return false;
		}

	}

	public function login($error = false)
	{
		if ($this->ion_auth->logged_in()==TRUE)
		{
			return redirect('account/info');
		}
		if($this->input->server('REQUEST_METHOD')=="POST") {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('email', 'email', 'trim|required|min_length[5]|valid_email');
			$this->form_validation->set_rules('password', 'password', 'trim|required');
			$this->form_validation->set_rules('g-recaptcha-response', 'g-recaptcha-response', 'trim|required|callback_validate_captcha');
			$rememberme = ($this->input->post('rememberme')=="true") ? TRUE : FALSE;
			if ($this->form_validation->run() == TRUE) {
				if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'), $rememberme)==TRUE) {
					return redirect('account/info','refresh');
				} else {
					flash('E-Mail anda Atau Password anda Salah!');
				}
			}
		}
		$this->load->helper('form');
		$html['title'] = "Login";
		return view('pages/accounts/login', $html);
	}

	public function register($error = false)
	{
		if ($this->ion_auth->logged_in()==TRUE)
		{
			return redirect('account/info');
		}
		$this->load->library('form_validation');
		if ($this->input->server('REQUEST_METHOD')=="POST") {
			$this->form_validation->set_rules('nama', 'nama Fields', 'trim|required|min_length[5]|max_length[50]');
			$this->form_validation->set_rules('birthday', 'birthday Fields', 'trim|required|min_length[5]|max_length[10]');
			$this->form_validation->set_rules('gender', 'gender Fields', 'trim|required|in_list[male,female]');
			$this->form_validation->set_rules('phone', 'Phone Fields', 'trim|required|min_length[10]|max_length[15]|numeric');
			$this->form_validation->set_rules('useremail', 'email Fields', 'trim|required|min_length[5]|max_length[50]|valid_email|is_unique[users.email]');
			$this->form_validation->set_rules('confemail', 'Confirmation Email Field', 'trim|required|matches[useremail]|min_length[5]|max_length[50]|valid_email');
			$this->form_validation->set_rules('password', 'Password Field', 'trim|required|min_length[8]|max_length[32]');
			$this->form_validation->set_rules('confpassword', 'Confirmation Passwords Field', 'trim|required|min_length[8]|max_length[32]|matches[password]');
			$this->form_validation->set_rules('g-recaptcha-response', 'g-recaptcha-response', 'trim|required|callback_validate_captcha');
			if ($this->form_validation->run() == TRUE) {
				$insert_users = $this->ion_auth->register($this->input->post('useremail'), $this->input->post('password'), $this->input->post('useremail'), [
						'full_name' => $this->input->post('nama'),
						'birthday' => $this->input->post('birthday'),
						'phone' => $this->input->post('phone')
					], ['4']);
				if ($insert_users == false) {
					flash('Errors Memasukan Data Ke Database!', 'alert');
				} else {
					flash('Berhasil Mendaftar Silahkan Verifikasi E-mail anda', 'success');
					return redirect('login');
				}
			}
		}
		$this->load->model('Provinces');
		$html['provinces'] = $this->Provinces->select_all();
		$html['header'] = "
		<link rel='stylesheet' href=".assets('vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css').">
		";
		$html['footer'] = "
		<script src=".assets('vendor/moment/moment.js')." type='text/javascript' charset='utf-8'></script>
		<script src=".assets('vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')." type='text/javascript' charset='utf-8'></script>
		<script type='text/javascript' charset='utf-8'>
			$('[name=province]').change(function () {
	        	 $.ajax({
    				url:'".base_url('ajax/register/regencies')."',
    				type: 'post',
    				data: {
							regencies: this.value
						}, 
    				success:function(data) {
    					if (data.success==0) {
    						sweetAlert('Ouchhh...', 'Gagal Mengambil Data!', 'error');
    					} else {
    						$('[name=regencies]').html('<option value=0>== Pilih Kabupaten ==</option>');
    						$('[name=districts]').html('<option value=0>== Pilih Kecamatan ==</option>');
    						$('[name=villages]').html('<option value=0>== Pilih Kelurahan ==</option>');
    						$('[name=regencies]').append(data.html);
    					}
    				}
  				});
    		});
    		$('[name=regencies]').change(function () {
	        	 $.ajax({
    				url:'".base_url('ajax/register/districts')."',
    				type: 'post',
    				data: {
							districts: this.value
						}, 
    				success:function(data) {
    					if (data.success==0) {
    						sweetAlert('Ouchhh...', 'Gagal Mengambil Data!', 'error');
    					} else {
    						$('[name=districts]').empty();
    						$('[name=villages]').empty();
    						$('[name=districts]').append(data.html);
    					}
    				}
  				});
    		});
    		$('[name=districts]').change(function () {
	        	 $.ajax({
    				url:'".base_url('ajax/register/villages')."',
    				type: 'post',
    				data: {
							villages: this.value
						}, 
    				success:function(data) {
    					if (data.success==0) {
    						sweetAlert('Ouchhh...', 'Gagal Mengambil Data!', 'error');
    					} else {
    						$('[name=villages]').empty();
    						$('[name=villages]').append(data.html);
    					}
    				}
  				});
    		});
    	$(function () {
            $('#date').datetimepicker({
            	format: 'YYYY-MM-DD',
            });
        });
    </script>
		";
		return view('pages/accounts/register', $html);
	}

	public function ajax_register($type = 'null')
	{
		header("Access-Control-Allow-Methods: *");
		header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
		$return = '';
		if ($this->input->is_ajax_request()==TRUE) {
			if ($type=="regencies") {
				$this->load->model('regencies');
				$regencies = $this->regencies->ProvinceId($_POST['regencies']);
				$return['html'] = '';
				foreach ($regencies as $regency) {
					$return['html'] .= '<option value="'.$regency->id.'">'.$regency->name.'</option>';
				}
				$return['success'] = 1;
			} elseif ($type == "districts") {
				if ($this->input->is_ajax_request()==TRUE) {
					$this->load->model('districts');
					$districts = $this->districts->RegencyId($_POST['districts']);
					$return['html'] = '<option value="0">== Pilih Kecamatan ==</option>';
					foreach ($districts as $district) {
						$return['html'] .= '<option value="'.$district->id.'">'.$district->name.'</option>';
					}
					$return['success'] = 1;

				}
			} elseif ($type == "villages") {
				if ($this->input->is_ajax_request()==TRUE) {
					$this->load->model('villages');
					$villages = $this->villages->districtsId	($_POST['villages']);
					$return['html'] = '<option value="0">== Pilih Kelurahan ==</option>';
					foreach ($villages as $village) {
						$return['html'] .= '<option value="'.$village->id.'">'.$village->name.'</option>';
					}
					$return['success'] = 1;

				}
			} else {
				show_404();
			}
		return $this->output
            ->set_content_type('application/json')
            ->set_output(
            	json_encode(
            		$return
            	)
            );
        }
	}

	public function logout()
	{
		$this->ion_auth->logout();
		redirect_back();
	}

	public function forgot()
	{
		
	}

	public function info()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('login');
		}
		$html['user'] = $this->ion_auth->user()->row();
		return view('pages/accounts/info', $html);
	}

	public function change_password()
	{
		// echo Carbon::now()->subDays(5)->diffForHumans();
	}

	public function wishlist()
	{
		
	}

	// public function 

}

/* End of file AccountControllers.php */
/* Location: ./application/controllers/AccountControllers.php */