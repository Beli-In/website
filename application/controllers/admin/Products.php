<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Products extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        if ($this->ion_auth->is_admin()==false) {
            flash("You Don't Have Authorization to Access This Level");
            return redirect('_admin/login');
        }
        $this->load->model(['Brand', 'Categories', 'Products_model', 'ProductsHaveCategories', 'Productsimage']);
        $this->load->helper('form');
    }

    public function index($page = 0)
    {
        if ($this->input->server('REQUEST_METHOD')=="POST" && $this->input->post('method')=="delete") {
            $id = xssclear($this->input->post('id'));
            $product = $this->Products_model->selectbyId($id);
            if ($this->Products_model->deletebyId($id)==true) {
                $this->Productsimage->deleteByKey($product->image_key);
                return 'success';
            }
            return 'failed';
        }
        $this->load->library('pagination');
        $config['base_url'] = base_url('_admin/products/index');
        $config['per_page'] = 10;
        $html["products"] = $this->Products_model->select_all($config["per_page"], $page);
        $config['total_rows'] = $this->Products_model->count();
        $config['num_links'] = 5;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4))? $this->uri->segment(4) : 0;

        $html["pagination"] = $this->pagination->create_links();
        // var_dump($html['pagination']);

        // Pages Metas
        $html['title'] = "List Products";
        $html['header'] = "
        <!-- Sweet Alert -->
        <link href=".privassets('plugins/bootstrap-sweetalert/sweet-alert.css')." rel='stylesheet' type='text/css'>
        ";
        $html['footer'] = "
        <!-- Sweet-Alert  -->
        <script src=".privassets('plugins/bootstrap-sweetalert/sweet-alert.min.js')."></script>
        <script>
            $('.deletebutton').click(function(){
                var ids = $(this).data('product-id');
                swal({   
                    title: 'Are you sure?',   
                    text: 'You will not be able to recover this imaginary file!',   
                    type: 'warning',   
                    showCancelButton: true,   
                    confirmButtonColor: '#DD6B55',   
                    confirmButtonText: 'Yes, delete it!',   
                    closeOnConfirm: false 
                }, function(){
                    console.log(ids);
                    $.post('".base_url('_admin/products')."', {id: ids, method : 'delete'});
                    swal('Deleted!', 'Your imaginary file has been deleted.', 'success');
                    location.reload();
                });
            });
        </script>
        ";
        adview('admin/pages/products/index', $html);
    }

    private function _form()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama-product', 'nama products', 'required|trim|min_length[5]|max_length[100]');
        $this->form_validation->set_rules('brand', 'brand', 'required|trim|integer');
        $this->form_validation->set_rules('category[]', 'category', 'required');
        $this->form_validation->set_message('multiply_categories', "Category Harus Di Pilih !");
        $this->form_validation->set_rules('feature', 'feature products', 'trim|max_length[450]');
        $this->form_validation->set_rules('deskripsi', 'deskripsi products', 'trim');
        $this->form_validation->set_rules('price', 'harga product', 'trim|required');
        $this->form_validation->set_rules('stock', 'stock product', 'trim|required');
        $this->form_validation->set_rules('weight', 'berat products', 'trim|required');
        if ($this->form_validation->run() == true) {
            return true;
        } else {
            return false;
        }
    }

    public function create()
    {
        if ($this->session->userdata('products-upload')==true && $this->session->userdata('updatedproduct')==false) {
            $photouploads_db = $this->Productsimage->selectByKey($this->session->userdata('products-upload'));
            if ($photouploads_db!==false) {
                $filesuploads = "[";
                foreach ($photouploads_db as $photoupload) {
                    $filesuploads .= "{
                        name: '".$photoupload->filename."',
                        size: ".$photoupload->size.",
                        type: 'image/".$photoupload->extension."',
                        file: '".$photoupload->path."',
                        url: '".base_url($photoupload->path)."',
                    },";
                }
                $filesuploads .= "]";
            } else {
                $filesuploads = 'null';
            }
        } else {
            $filesuploads = 'null';
        }
        if ($this->input->server('REQUEST_METHOD')=="POST") {
            if ($this->_form()==true) {
                $data = $this->Products_model->insert([
                    'slug' => url_title($this->input->post('nama-product'), 'dash', true),
                    'image_key' =>  $this->session->userdata('products-upload'),
                    'name' => $this->input->post('nama-product'),
                    'brands_id' => $this->input->post('brand'),
                    'price' => filter_var($this->input->post('price'), FILTER_SANITIZE_NUMBER_INT),
                    'weight' => filter_var($this->input->post('weight'), FILTER_SANITIZE_NUMBER_INT),
                    'stock' => filter_var($this->input->post('stock'), FILTER_SANITIZE_NUMBER_INT),
                    'feature' => $this->input->post('feature'),
                    'detail' => $this->input->post('deskripsi'),
                    'publish' => ($this->input->post('publish')!=="Y") ? "N" : "Y",
                    'created_at' => null
                ]);
                if ($data==false) {
                    $this->Productsimage->deleteByKey($this->session->userdata('products-upload'));
                    flash('gagal Input Data', 'danger');
                } else {
                    $this->ProductsHaveCategories->insertProducts($data['last_id'], $this->input->post('category'));
                    flash('berhasil input Product '.$data['name'], 'success');
                }
            } else {
                $this->Productsimage->deleteByKey($this->session->userdata('products-upload'));
            }
            if ($this->session->userdata('products-upload')==true) {
                $this->session->unset_userdata('products-upload');
            }
        }
        $html['header'] = "
        <!-- Jquery filer css -->
        <link href=".privassets('plugins/jquery.filer/css/jquery.filer.css')." rel='stylesheet' />
        <link href=".privassets('plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css')." rel='stylesheet' />
        <script type='text/javascript'>
            var site_url = '".base_url()."';
            var filesuploads = ".$filesuploads.";
        </script>
        ";
        $html['footer'] = "
        <!-- Jquery filer js -->
        <script src=".privassets('plugins/jquery.filer/js/jquery.filer.min.js')."></script>
        <script type='text/javascript' src=".privassets('/plugins/parsleyjs/parsley.min.js')."></script>

        <script src=".privassets('plugins/autoNumeric/autoNumeric.js')." type='text/javascript'></script>

        <!--Wysiwig js-->
        <script src=".privassets('plugins/tinymce/tinymce.min.js')."></script>
        <script type='text/javascript'>
        	$(document).ready(function () {
                $('form').parsley();
			    if($('.texteditor').length > 0){
			        tinymce.init({
			            selector: 'textarea.texteditor',
			            theme: 'modern',
			            height:300,
			            plugins: [
			                'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
			                'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
			                'save table contextmenu directionality emoticons template paste textcolor'
			            ],
			            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
			            style_formats: [
			                {title: 'Bold text', inline: 'b'},
			                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
			                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
			                {title: 'Example 1', inline: 'span', classes: 'example1'},
			                {title: 'Example 2', inline: 'span', classes: 'example2'},
			                {title: 'Table styles'},
			                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
			            ]
			        });
			    }
			});
            jQuery(function($) {
		      $('.autonumber').autoNumeric('init');
		  });
        </script>
        <!-- page specific js -->
        <script src=".privassets('pages/jquery.fileuploads.init.js')."></script>
        ";
        $html['title'] = "Create Products";
        $html['brands'] = $this->Brand->select_all();
        $html['categories'] = $this->Categories->select_all();
        adview('admin/pages/products/create', $html);
    }



    public function search()
    {
        if ($this->input->server('REQUEST_METHOD')=="GET") {
            $this->load->library('form_validation');
            $this->load->library(['pagination']);
            $this->form_validation->set_data($this->input->get());
            $this->form_validation->set_rules('product', 'Nama product', 'trim|required');
            $search = xssclear($this->input->get('product'));
            if ($this->form_validation->run()==true || !empty($search)) {
                $this->load->model('Productsimage');
                $config['total_rows'] = $this->Products_model->countbyName($search);
                $config['per_page']  = 10;
                $config['page_query_string'] = true;
                $config['num_links'] = 5;
                $this->pagination->initialize($config);
                $page = (is_numeric($this->input->get('page'))) ? $this->input->get('page') : 0;
                $html["products"] = $this->Products_model->searchByname($search, $config["per_page"], $page);
                $html["pagination"] = $this->pagination->create_links();
                // var_dump($html['pagination']);
                $html['search'] = $search;
                // Pages Metas
                $html['title'] = "List Products ".$search;
                $html['header'] = "
                ";
                $html['footer'] = "
                ";
                return adview('admin/pages/products/search', $html);
            } else {
                flash('Silahkan Masukan Nama Product yang ingin anda cari');
                redirect('_admin/products');
            }
        } else {
            flash('Silahkan Masukan Nama Product yang ingin anda cari');
            redirect('_admin/products');
        }
    }

    public function edit($id)
    {
        $id = xssclear($id);
        $html['product'] = $this->Products_model->selectbyId($id);
        if ($html['product']==false) {
            flash('Product Tidak ditemukan!');
            return redirect('_admin/products');
        }
        $array = array(
            'products-upload' => $html['product']->image_key,
            'updatedproduct' => $html['product']->image_key
        );
        $this->session->set_userdata($array);
        if ($this->input->server('REQUEST_METHOD')=="POST") {
            if ($this->_form()==true) {
                $data = $this->Products_model->update([
                    'slug' => url_title($this->input->post('nama-product'), 'dash', true),
                    'image_key' =>  $this->session->userdata('products-upload'),
                    'name' => $this->input->post('nama-product'),
                    'brands_id' => $this->input->post('brand'),
                    'price' => filter_var($this->input->post('price'), FILTER_SANITIZE_NUMBER_INT),
                    'weight' => filter_var($this->input->post('weight'), FILTER_SANITIZE_NUMBER_INT),
                    'stock' => filter_var($this->input->post('stock'), FILTER_SANITIZE_NUMBER_INT),
                    'feature' => $this->input->post('feature'),
                    'detail' => $this->input->post('deskripsi'),
                    'publish' => ($this->input->post('publish')!=="Y") ? "N" : "Y",
                    'created_at' => null
                ], $html['product']->id);
                if ($data==false) {
                    $this->Productsimage->deleteByKey($this->session->userdata('products-upload'));
                    flash('gagal Input Data', 'danger');
                } else {
                    $this->ProductsHaveCategories->updatedProducts($html['product']->id, $this->input->post('category'));
                    flash('berhasil input Product '.$data['name'], 'success');
                }
            } else {
                $this->Productsimage->deleteByKey($this->session->userdata('products-upload'));
            }
            if ($this->session->userdata('products-upload')==true) {
                $this->session->unset_userdata('products-upload');
                $this->session->unset_userdata('updatedproduct');
            }
            if ($data!==false) {
                return redirect('_admin/products');
            }
        }
        $html['brands'] = $this->Brand->select_all();
        $html['categories'] = $this->Categories->select_all();
        if ($this->session->userdata('products-upload')==true && $this->session->userdata('updatedproduct')==$html['product']->image_key) {
            $photouploads_db = $this->Productsimage->selectByKey($html['product']->image_key);
            if ($photouploads_db!==false) {
                $filesuploads = "[";
                foreach ($photouploads_db as $photoupload) {
                    $filesuploads .= "{
                    name: '".$photoupload->filename."',
                    size: ".$photoupload->size.",
                    type: 'image/".$photoupload->extension."',
                    file: '".$photoupload->path."',
                    url: '".base_url($photoupload->path)."',
                },";
                }
                $filesuploads .= "]";
            } else {
                $filesuploads = 'null';
            }
        } else {
            $filesuploads = 'null';
        }
        $html['header'] = "
        <!-- Jquery filer css -->
        <link href=".privassets('plugins/jquery.filer/css/jquery.filer.css')." rel='stylesheet' />
        <link href=".privassets('plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css')." rel='stylesheet' />
        <script type='text/javascript'>
            var site_url = '".base_url()."';
            var filesuploads = ".$filesuploads.";
        </script>
        ";
        $html['footer'] = "
        <!-- Jquery filer js -->
        <script src=".privassets('plugins/jquery.filer/js/jquery.filer.min.js')."></script>
        <script type='text/javascript' src=".privassets('/plugins/parsleyjs/parsley.min.js')."></script>

        <script src=".privassets('plugins/autoNumeric/autoNumeric.js')." type='text/javascript'></script>

        <!--Wysiwig js-->
        <script src=".privassets('plugins/tinymce/tinymce.min.js')."></script>
        <script type='text/javascript'>
            $(document).ready(function () {
                $('form').parsley();
                if($('.texteditor').length > 0){
                    tinymce.init({
                        selector: 'textarea.texteditor',
                        theme: 'modern',
                        height:300,
                        plugins: [
                            'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                            'save table contextmenu directionality emoticons template paste textcolor'
                        ],
                        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
                        style_formats: [
                            {title: 'Bold text', inline: 'b'},
                            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                            {title: 'Example 1', inline: 'span', classes: 'example1'},
                            {title: 'Example 2', inline: 'span', classes: 'example2'},
                            {title: 'Table styles'},
                            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                        ]
                    });
                }
            });
            jQuery(function($) {
              $('.autonumber').autoNumeric('init');
          });
        </script>
        <!-- page specific js -->
        <script src=".privassets('pages/jquery.fileuploads.init.js')."></script>
        ";
        $html['title'] = "Edit Products ".xssclear($html['product']->name);
        return adview('admin/pages/products/edit', $html);
    }

    public function delete()
    {
    }
}
