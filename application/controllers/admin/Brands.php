<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Brands extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    //Codeigniter : Write Less Do More
    }

    public function index()
    {
    }

    public function create()
    {

        return adview('admin/pages/brands/index');
    }
}
