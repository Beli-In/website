<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pages extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
    }

    private function Auth()
    {
        if ($this->ion_auth->is_admin()==false) {
            flash("You Don't Have Authorization to Access This Level");
            return redirect('_admin/login');
        }
    }

    public function validate_captcha($str)
    {
        $recaptcha = new \ReCaptcha\ReCaptcha($this->config->item('re-privatekey'));
        $resp = $recaptcha->verify($str, $this->input->server('REMOTE_ADDR'));
        $this->form_validation->set_message('validate_captcha', "Captcha Fields is Required !");
        if ($resp->isSuccess()) {
            return true;
        } else {
            return false;
        }
    }


    public function home()
    {
        $this->Auth();
        return adview('admin/pages/index');
    }

    public function login()
    {
        $this->load->helper('form');
        if ($this->input->server('REQUEST_METHOD') == "POST") {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email', 'email', 'trim|required|min_length[5]|max_length[20]');
            $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[8]|max_length[50]');
            // $this->form_validation->set_rules('g-recaptcha-response', 'g-recaptcha-response', 'trim|required|callback_validate_captcha');
            $rememberme = ($this->input->post('rememberme')=="true") ? true : false;
            if ($this->form_validation->run() == true) {
                if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'), $rememberme)==true) {
                    if ($this->ion_auth->is_admin()==false) {
                        $this->ion_auth->logout();
                        flash("You Don't Have Authorization to Access This Level");
                    } else {
                        redirect('_admin/home');
                    }
                } else {
                    flash('Email or Password is Not Correct!');
                }
            }
        }
        adview('admin/pages/login', false, null);
    }

    public function logout()
    {
        $this->ion_auth->logout();
        flash('Successfuly Logout', 'success');
        redirect('_admin/login');
    }
}
