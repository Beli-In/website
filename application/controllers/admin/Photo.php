<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Photo extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        if ($this->ion_auth->is_admin()==false || $this->input->is_ajax_request()==false) {
            flash("You Don't Have Authorization to Access This Level");
            return redirect('_admin/login');
        }
        $this->load->helper('file');
        $this->load->model('Productsimage');
    }

    public function index()
    {
    }

    public function resize($width = 80, $height = 80, $files)
    {
        $config = array();
            $config['image_library'] = 'gd2';
            $config['source_image'] = $files['metas'][0]['file'];
            $config['create_thumb'] = true;
            $config['new_image'] = 'media/thumbs/';
            $config['maintain_ratio'] = false;
            $config['thumb_marker'] = '';
            $config['width'] = $width;
            $config['height'] = 80;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
    }
    /*
    *   Upload File
    *   @return json
     */
    public function do_upload()
    {
        if ($this->session->userdata('products-upload')==false) {
            $array = array(
                    'products-upload' => substr(bin2hex(random_bytes(25)), 0, 25),
                );
            $this->session->set_userdata($array);
        }
        $this->load->library('Uploader');
        $data = $this->uploader->upload($_FILES['product_photos'], array(
           'limit' => 5, //Maximum Limit of files. {null, Number}
           'maxSize' => 25, //Maximum Size of files {null, Number(in MB's)}
           'extensions' => ['jpg', 'png', 'jpeg'], //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
           'required' => true, //Minimum one file is required for upload {Boolean}
           'uploadDir' => 'media/'.date('Y\/m\/'), //Upload directory {String}
           'title' => '{{timestamp}}_{{random}}', //New file name {null, String, Array} *please read documentation in README.md
           'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
           'replace' => false, //Replace the file if it already exists  {Boolean}
           'perms' => 0644, //Uploaded file permisions {null, Number}
           'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
           'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
           'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
           'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
           'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
           'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
       ));

        if ($data['isComplete']) {
            $files = $data['data'];
            $this->resize(80, 80, $files);
            
            function get_numerics($str)
            {
                return str_replace('.', '', $str);
            }
            $this->load->model('Productsimage');
            $this->Productsimage->insert([
                'key' => $this->session->userdata('products-upload'),
                'extension' => $files['metas'][0]['extension'],
                'path' => $files['metas'][0]['file'],
                'filename' => $files['metas'][0]['name'],
                'size' => get_numerics($files['metas'][0]['size2']),
                'filename' => $files['metas'][0]['name'],
                'created_at' => null
            ]);
            echo json_encode($files['metas'][0]['name']);
        }
        if ($data['hasErrors']) {
            $errors = $data['errors'];
            echo json_encode($errors);
        }
    }

    public function deleteimage()
    {
        if ($this->input->server('REQUEST_METHOD')!=="POST" || $this->input->post('file')==false) {
            return show_404();
        }
        $file = $this->input->post('file');
        //gets the job done but you might want to add error checking and security
        function deletePhotos($photo)
        {
            //Check if the "Owner of the Photo" is the "User Currently Logged in"
            //Usuually something like $logged_in_user == $owner_of_photo
                //More Hoops and Loops they shall JUMP OVER!
                //This var will be compare to $photo and $thumbnail var to always check that one of these words exist in the var
                $allowedFiles = array("png","jpg","jpeg");

                //This will grab the "extension" of the file
                $grabExt = substr($photo, strrpos($photo, '.') + 1);

                //Now we compare and we also make sure it EXIST
                if (!in_array(strtolower($photo), $allowedFiles) && file_exists($photo)) {
                    //Delete the File
                    return unlink($photo);
                } else {
                    //If they can't jump over the hoops, send them to HELL!
                    return false;
                }
        }
        $success = deletePhotos(FCPATH . 'media/' . $file);
        $success = deletePhotos(FCPATH . 'media/' . $file.'_thumb');
        //info to see if it is doing what it is supposed to
        $this->Productsimage->deleteByName($file);
        $info = new StdClass;
        $info->success = $success;
        $info->path = base_url('media/') . $file;
        $info->file = is_file(FCPATH . 'media/' . $file);

        //I don't think it matters if this is set but good for error checking in the console/firebug
        echo json_encode(array($info));
    }
}
