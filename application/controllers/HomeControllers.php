<?php
defined('BASEPATH') or exit('No direct script access allowed');

class HomeControllers extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Products_model', 'Productsimage']);
    }

    public function index()
    {
        $html['products'] = $this->Products_model->select_all(8);
        $this->load->view('pages/home', $html);
    }
}

/* End of file HomeControllers.php */
/* Location: ./application/controllers/HomeControllers.php */
