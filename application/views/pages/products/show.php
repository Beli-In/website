<section class="shop-details-area">
    <div class="breadcrumbs">
        <div class="container">
            <div class="container-inner">
                <ul>
                    <li class="home">
                        <a href="#">Home</a>
                        <span>
                            <i class="fa fa-angle-right"></i>
                        </span>
                    </li>
                    <li class="home-two">
                        <a href="#">Smartphones & Accessories</a>
                        <span>
                            <i class="fa fa-angle-right"></i>
                        </span>
                    </li>
                    <li class="category3">
                        <strong>Cras neque metus</strong>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="shop-details">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 hidden-xs">
                    <div class="s_big">
                        <div>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div id="image1" class="tab-pane fade in active">
                                    <div class="simpleLens-big-image-container">
                                        <a class="simpleLens-lens-image" data-lens-image="img/product/4.jpg">
                                            <img alt="" src="img/product/4.jpg" class="simpleLens-big-image">
                                        </a>
                                    </div>
                                </div>
                                <div id="image2" class="tab-pane fade">
                                    <div class="simpleLens-big-image-container">
                                        <a class="simpleLens-lens-image" data-lens-image="img/product/5_1_1.jpg">
                                            <img alt="" src="img/product/5_1_1.jpg" class="simpleLens-big-image">
                                        </a>
                                    </div>
                                </div>
                                <div id="image3" class="tab-pane fade">
                                    <div class="simpleLens-big-image-container">
                                        <a class="simpleLens-lens-image" data-lens-image="img/product/3_2.jpg">
                                            <img alt="" src="img/product/3_2.jpg" class="simpleLens-big-image">
                                        </a>
                                    </div>
                                </div>
                                <div id="image4" class="tab-pane fade">
                                    <div class="simpleLens-big-image-container">
                                        <a class="simpleLens-lens-image" data-lens-image="img/product/17.jpg" >
                                            <img alt="" src="img/product/17.jpg" class="simpleLens-big-image">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="thumnail-image fix">
                                <ul class="tab-menu">
                                    <li class="active"><a data-toggle="tab" href="#image1"><img alt="" src="img/product/4.jpg"></a></li>
                                    <li><a data-toggle="tab" href="#image2"><img alt="" src="img/product/5_1_1.jpg" ></a></li>
                                    <li><a data-toggle="tab" href="#image3"><img alt="" src="img/product/3_2.jpg"></a></li>
                                    <li><a data-toggle="tab" href="#image4"><img alt="" src="img/product/17.jpg"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="cras">
                        <div class="product-name">
                            <h1>Cras neque metus</h1>
                        </div>
                        <div class="pro-rating">
                            <div class="pro_one">
                                <a href="#">
                                    <i class="fa fa-star"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-star"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-star"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-star"></i>
                                </a>
                            </div>
                            <div class="pro_two">
                                <a href="#">
                                    <i class="fa fa-star-o"></i>
                                </a>
                            </div>
                            <p class="rating-links">
                                <a href="#">1 Review(s)</a>
                                <span class="separator">|</span>
                                <a href="#">Add Your Review</a>
                            </p>
                        </div>
                        <p class="availability in-stock">
                            Availability:
                            <span>In stock</span>
                        </p>
                        <div class="short-description">
                            <p> Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti</p>
                        </div>
                        <div class="pre-box">
                            <span class="special-price">$155.00</span>
                        </div>
                        <div class="add-to-box1">
                            <div class="add-to-box add-to-box2">
                                <div class="add-to-cart">
                                    <div class="input-content">
                                        <label for="qty">Quantity:</label>
                                        <input id="qty" class="input-text qty" type="text" title="Qty" value="1" maxlength="12" name="qty">
                                    </div>
                                    <button class="button2 btn-cart" onclick="productAddToCartForm.submit(this)" title="" type="button">
                                    <span>Add to Cart</span>
                                    </button>
                                </div>
                                <div class="product-icon">
                                    <a href="#">
                                        <i class="fa fa-heart"> </i>
                                    </a>
                                    <a href="#">
                                        <i class="fa fa-retweet"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fa fa-envelope"> </i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="product-social">
                            <div class="addthis_toolbox">
                                <div class="pluginButtonLabel">
                                    <ul>
                                        <li class="blue"><a href="#"><i class="fa fa-facebook-official"></i>Like</a></li>
                                        <li class="zero"><a href="#">0</a></li>
                                        <li class="sky"><a href="#"><i class="fa fa-twitter"></i>tweet</a></li>
                                        <li class="puce"><a href="#">G+1</a></li>
                                        <li class="zero"><a href="#">0</a></li>
                                        <li class="orange"><a href="#"><i class="fa fa-plus-square"></i>Share</a>
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook-official"></i>Facebook</a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i>tweet</a></li>
                                            <li><a href="#"><i class="fa fa-envelope-square"></i>Email</a></li>
                                            <li><a href="#"><i class="fa fa-print"></i>Print</a></li>
                                            <li><a href="#"><i class="fa fa-envelope-o"></i>Gmail</a></li>
                                            <li><a href="#"><i class="fa fa-certificate"></i>Favorits</a></li>
                                            <li><a href="#"><i class="fa fa-plus-square"></i>More...(274)</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="ma-title">
                    <h2> Related Products </h2>
                </div>
                <div class="all">
                    <p>Check items to add to the cart or<a href="#">select all</a></p>
                    <div class=" content_top content_all indicator-style">
                        <div class="ma-box-content-all">
                            <div class="ma-box-content">
                                <div class="product-img-right">
                                    <input class="check" type="checkbox" required="">
                                    <a href="#">
                                        <img class="primary-image" alt="" src="img/product/8.jpg">
                                    </a>
                                </div>
                                <div class="product-content">
                                    <h2 class="product-name">
                                    <a href="#">Accumsan elit </a>
                                    </h2>
                                    <div class="pro-rating">
                                        <div class="pro_one">
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star-half-o"></i>
                                            </a>
                                        </div>
                                        <div class="pro_two proin">
                                            <a href="#">
                                                <i class="fa fa-star-o"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="price-box">
                                        <span class="special">$333.00</span>
                                    </div>
                                </div>
                            </div>
                            <div class="ma-box-content">
                                <div class="product-img-right">
                                    <input class="check" type="checkbox" required="">
                                    <a href="#">
                                        <img class="primary-image" alt="" src="img/product/4.jpg">
                                    </a>
                                </div>
                                <div class="product-content">
                                    <h2 class="product-name">
                                    <a href="#">Nunc facilisis</a>
                                    </h2>
                                    <div class="pro-rating">
                                        <div class="pro_one">
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star-half-o"></i>
                                            </a>
                                        </div>
                                        <div class="pro_two">
                                            <a href="#">
                                                <i class="fa fa-star-o"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="price-box">
                                        <span class="special">$222.00</span>
                                        <span class="old">$333.00</span>
                                    </div>
                                </div>
                            </div>
                            <div class="ma-box-content">
                                <div class="product-img-right">
                                    <input class="check" type="checkbox" required="">
                                    <a href="#">
                                        <img class="primary-image" alt="" src="img/product/5_1_1.jpg">
                                    </a>
                                </div>
                                <div class="product-content">
                                    <h2 class="product-name">
                                    <a href="#">consequences</a>
                                    </h2>
                                    <div class="pro-rating">
                                        <div class="pro_one">
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star-half-o"></i>
                                            </a>
                                        </div>
                                        <div class="pro_two" >
                                            <a href="#">
                                                <i class="fa fa-star-o"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="price-box">
                                        <span class="special">$211.00</span>
                                        <span class="old">$333.00</span>
                                    </div>
                                </div>
                            </div>
                            <div class="ma-box-content">
                                <div class="product-img-right">
                                    <input class="check" type="checkbox" required="">
                                    <a href="#">
                                        <img class="primary-image" alt="" src="img/product/4.jpg">
                                    </a>
                                </div>
                                <div class="product-content">
                                    <h2 class="product-name">
                                    <a href="#">Nunc facilisis</a>
                                    </h2>
                                    <div class="pro-rating">
                                        <div class="pro_one">
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star-half-o"></i>
                                            </a>
                                        </div>
                                        <div class="pro_two">
                                            <a href="#">
                                                <i class="fa fa-star-o"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="price-box">
                                        <span class="special">$222.00</span>
                                        <span class="old">$333.00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ma-box-content-all">
                            <div class="ma-box-content">
                                <div class="product-img-right">
                                    <input class="check" type="checkbox" required="">
                                    <a href="#">
                                        <img class="primary-image" alt="" src="img/product/15_1.jpg">
                                    </a>
                                </div>
                                <div class="product-content">
                                    <h2 class="product-name">
                                    <a href="#">Primis in faucibus</a>
                                    </h2>
                                    <div class="pro-rating">
                                        <div class="pro_one">
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star-half-o"></i>
                                            </a>
                                        </div>
                                        <div class="pro_two proin">
                                            <a href="#">
                                                <i class="fa fa-star-o"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="price-box">
                                        <span class="special">$99.00</span>
                                    </div>
                                </div>
                            </div>
                            <div class="ma-box-content">
                                <div class="product-img-right">
                                    <input class="check" type="checkbox" required="">
                                    <a href="#">
                                        <img class="primary-image" alt="" src="img/product/4.jpg">
                                    </a>
                                </div>
                                <div class="product-content">
                                    <h2 class="product-name">
                                    <a href="#">Nunc facilisis</a>
                                    </h2>
                                    <div class="pro-rating">
                                        <div class="pro_one">
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star-half-o"></i>
                                            </a>
                                        </div>
                                        <div class="pro_two">
                                            <a href="#">
                                                <i class="fa fa-star-o"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="price-box">
                                        <span class="special">$222.00</span>
                                        <span class="old">$333.00</span>
                                    </div>
                                </div>
                            </div>
                            <div class="ma-box-content">
                                <div class="product-img-right">
                                    <input class="check" type="checkbox" required="">
                                    <a href="#">
                                        <img class="primary-image" alt="" src="img/product/8.jpg">
                                    </a>
                                </div>
                                <div class="product-content">
                                    <h2 class="product-name">
                                    <a href="#">Nunc facilisis</a>
                                    </h2>
                                    <div class="pro-rating">
                                        <div class="pro_one">
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star-half-o"></i>
                                            </a>
                                        </div>
                                        <div class="pro_two">
                                            <a href="#">
                                                <i class="fa fa-star-o"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="price-box">
                                        <span class="special">$222.00</span>
                                        <span class="old">$333.00</span>
                                    </div>
                                </div>
                            </div>
                            <div class="ma-box-content">
                                <div class="product-img-right">
                                    <input class="check" type="checkbox" required="">
                                    <a href="#">
                                        <img class="primary-image" alt="" src="img/product/17.jpg">
                                    </a>
                                </div>
                                <div class="product-content">
                                    <h2 class="product-name">
                                    <a href="#">Nunc facilisis</a>
                                    </h2>
                                    <div class="pro-rating">
                                        <div class="pro_one">
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-star-half-o"></i>
                                            </a>
                                        </div>
                                        <div class="pro_two">
                                            <a href="#">
                                                <i class="fa fa-star-o"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="price-box">
                                        <span class="special">$222.00</span>
                                        <span class="old">$333.00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- end main_slider_area
============================================ -->
<section class="tab_area">
<div class="container">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="text">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Product Description</a>
                    </li>
                    <li role="presentation">
                        <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Reviews</a>
                    </li>
                    <li role="presentation">
                        <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Product Tags</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum. Quisque in arcu id dui vulputate mollis eget non arcu. Aenean et nulla purus. Mauris vel tellus non nunc mattis lobortis. Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum. Quisque in arcu id dui vulputate mollis eget non arcu. Aenean et nulla purus. Mauris vel tellus non nunc mattis lobortis. </div>
                    <div role="tabpanel" class="tab-pane" id="profile">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="customer-reviews">
                                    <div class="customer-reviews-one">
                                        <p><a href="#">Plazathemes</a> <span>Review by</span> Plazathemes</p>
                                    </div>
                                    <div class="customer-reviews-two">
                                        <p>Quality</p>
                                        <div class="pro-rating">
                                            <div class="pro_one">
                                                <a href="#">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="customer-reviews-two">
                                        <p>Price</p>
                                        <div class="pro-rating pro-ra-two">
                                            <div class="pro_one">
                                                <a href="#">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                            </div>
                                            <div class="pro_two">
                                                <a href="#">
                                                    <i class="fa fa-star-o"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="customer-reviews-two">
                                        <p>Value</p>
                                        <div class="pro-rating pro-ra-two">
                                            <div class="pro_one">
                                                <a href="#">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                                <a href="#">
                                                </a>
                                            </div>
                                            <div class="pro_two">
                                                <a href="#">
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="date">
                                        <p>Plazathemes <small>(Posted on 9/11/2014)</small></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-add table-responsive">
                                    <form action="#">
                                        <div class="form-border">
                                            <div class="add-text">
                                                <h3>
                                                You're reviewing:
                                                <span>Cras neque metus</span>
                                                </h3>
                                                <h4>
                                                How do you rate this product?*
                                                </h4>
                                            </div>
                                            <table class="data-table">
                                                <tr>
                                                    <th></th>
                                                    <th>1 star</th>
                                                    <th>2 stars</th>
                                                    <th>3 stars</th>
                                                    <th>4 stars</th>
                                                    <th>5 stars</th>
                                                </tr>
                                                <tr>
                                                    <td class="one two">Quality</td>
                                                    <td><input type="radio" name="ratings" required></td>
                                                    <td><input type="radio" name="ratings" required></td>
                                                    <td><input type="radio" name="ratings" required></td>
                                                    <td><input type="radio" name="ratings" required></td>
                                                    <td><input type="radio" name="ratings" required></td>
                                                </tr>
                                                <tr>
                                                    <td class="one">Price</td>
                                                    <td><input type="radio" name="ratings" required></td>
                                                    <td><input type="radio" name="ratings" required></td>
                                                    <td><input type="radio" name="ratings" required></td>
                                                    <td><input type="radio" name="ratings" required></td>
                                                    <td><input type="radio" name="ratings" required></td>
                                                </tr>
                                                <tr>
                                                    <td class="one">Value</td>
                                                    <td><input type="radio" name="ratings" required></td>
                                                    <td><input type="radio" name="ratings" required></td>
                                                    <td><input type="radio" name="ratings" required></td>
                                                    <td><input type="radio" name="ratings" required></td>
                                                    <td><input type="radio" name="ratings" required></td>
                                                </tr>
                                            </table>
                                            <div class="input-one form-list">
                                                <label class="required">Nickname<em>*</em></label>
                                                <input type="text" class="email" required>
                                            </div>
                                            <div class="input-one">
                                                <label class="required">Summary of Your Review<em>*</em></label>
                                                <input type="text" class="email" required>
                                            </div>
                                            <div class="input-one">
                                                <label class="required">Review<em>*</em></label>
                                                <textarea class="email"></textarea>
                                            </div>
                                            <button class="button2 btn-cart btn-in" type="button" title="">
                                            <span>Submit Review</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="messages">
                        <div class="box-collateral">
                            <h3>Other people marked this product with these tags:</h3>
                            <p><a href="#">Clothing</a>(3)</p>
                        </div>
                        <div class="input-two">
                            <label class="required">Add Your Tags:</label>
                            <input type="text" class="email tags" required>
                            <button class="button2 btn-cart btn-a" type="button" title="">
                            <span>Add Tags</span>
                            </button>
                        </div>
                        <p class="note">Use spaces to separate tags. Use single quotes (') for phrases.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<section class="product_area">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="ma-title">
                <h2>
                UpSell Products
                </h2>
            </div>
            <div class="row">
                <div class="UpSell indicator-style">
                    <!-- single-product start -->
                    <div class=" col-md-3">
                        <div class="single-product">
                            <span class="sale-text">Sale</span>
                            <div class="product-img">
                                <a href="#">
                                    <img class="primary-image" src="img/product/8.jpg" alt="" />
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="price-box">
                                    <span class="special-price">$155.00</span>
                                    <span class="old-price">$170.00 </span>
                                </div>
                                <h2 class="product-name"><a href="#">Fusce aliquam</a></h2>
                                <div class="pro-rating">
                                    <div class="pro_one">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star-half-o"></i></a>
                                    </div>
                                    <div class="pro_two">
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                    </div>
                                </div>
                                <div class="product-icon">
                                    <a href="#"><i class="fa fa-shopping-cart"> </i></a>
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                    <a href="#"><i class="fa fa-refresh"> </i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single-product end -->
                    <!-- single-product start -->
                    <div class=" col-md-3">
                        <div class="single-product">
                            <span class="sale-text">Sale</span>
                            <div class="product-img">
                                <a href="#">
                                    <img class="primary-image" src="img/product/4.jpg" alt="" />
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="price-box">
                                    <span class="special-price">$699.00</span>
                                    <span class="old-price">$800.00 </span>
                                </div>
                                <h2 class="product-name"><a href="#">Quisque in arcu</a></h2>
                                <div class="pro-rating">
                                    <div class="pro_one">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                    <div class="pro_two">
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                    </div>
                                </div>
                                <div class="product-icon">
                                    <a href="#"><i class="fa fa-shopping-cart"> </i></a>
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                    <a href="#"><i class="fa fa-refresh"> </i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single-product end -->
                    <!-- single-product start -->
                    <div class=" col-md-3">
                        <div class="single-product">
                            <span class="sale-text">Sale</span>
                            <div class="product-img">
                                <a href="#">
                                    <img class="primary-image" src="img/product/5_1_1.jpg" alt="" />
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="price-box">
                                    <span class="special-price">$88.00</span>
                                    <span class="old-price">$99.00</span>
                                </div>
                                <h2 class="product-name"><a href="#">Proin lectus ipsum</a></h2>
                                <div class="pro-rating">
                                    <div class="pro_one">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                    <div class="pro_two">
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                    </div>
                                </div>
                                <div class="product-icon">
                                    <a href="#"><i class="fa fa-shopping-cart"> </i></a>
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                    <a href="#"><i class="fa fa-refresh"> </i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single-product end -->
                    <!-- single-product start -->
                    <div class=" col-md-3">
                        <div class="single-product">
                            <span class="sale-text">Sale</span>
                            <div class="product-img">
                                <a href="#">
                                    <img class="primary-image" src="img/product/3_2.jpg" alt="" />
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="price-box">
                                    <span class="special-price">$123.00</span>
                                </div>
                                <h2 class="product-name"><a href="#">Aliquam consequat</a></h2>
                                <div class="pro-rating">
                                    <div class="pro_one">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                    <div class="pro_two">
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                    </div>
                                </div>
                                <div class="product-icon">
                                    <a href="#"><i class="fa fa-shopping-cart"> </i></a>
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                    <a href="#"><i class="fa fa-refresh"> </i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single-product end -->
                    <!-- single-product start -->
                    <div class=" col-md-3">
                        <div class="single-product">
                            <span class="sale-text">Sale</span>
                            <div class="product-img">
                                <a href="#">
                                    <img class="primary-image" src="img/product/5_1_1.jpg" alt="" />
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="price-box">
                                    <span class="special-price">$721.00</span>
                                </div>
                                <h2 class="product-name"><a href="#">Donec non est</a></h2>
                                <div class="pro-rating">
                                    <div class="pro_one">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                    <div class="pro_two">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                </div>
                                <div class="product-icon">
                                    <a href="#"><i class="fa fa-shopping-cart"> </i></a>
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                    <a href="#"><i class="fa fa-refresh"> </i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single-product end -->
                    <!-- single-product start -->
                    <div class=" col-md-3">
                        <div class="single-product">
                            <span class="sale-text">Sale</span>
                            <div class="product-img">
                                <a href="#">
                                    <img class="primary-image" src="img/product/7_2.jpg" alt="" />
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="price-box">
                                    <span class="special-price">$222.00</span>
                                </div>
                                <h2 class="product-name"><a href="#"> Donec ac tempus </a></h2>
                                <div class="pro-rating">
                                    <div class="pro_one">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                    <div class="pro_two">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                </div>
                                <div class="product-icon">
                                    <a href="#"><i class="fa fa-shopping-cart"> </i></a>
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                    <a href="#"><i class="fa fa-refresh"> </i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" col-md-3">
                        <div class="single-product">
                            <span class="sale-text">Sale</span>
                            <div class="product-img">
                                <a href="#">
                                    <img class="primary-image" src="img/product/6_4.jpg" alt="" />
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="price-box">
                                    <span class="special-price">$333.00</span>
                                </div>
                                <h2 class="product-name"><a href="#">Pellentesque habitant </a></h2>
                                <div class="pro-rating">
                                    <div class="pro_one">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                    <div class="pro_two">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                </div>
                                <div class="product-icon">
                                    <a href="#"><i class="fa fa-shopping-cart"> </i></a>
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                    <a href="#"><i class="fa fa-refresh"> </i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" col-md-3">
                        <div class="single-product">
                            <span class="sale-text">Sale</span>
                            <div class="product-img">
                                <a href="#">
                                    <img class="primary-image" src="img/product/13.jpg" alt="" />
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="price-box">
                                    <span class="special-price">$432.00</span>
                                </div>
                                <h2 class="product-name"><a href="#">Etiam gravida</a></h2>
                                <div class="pro-rating">
                                    <div class="pro_one">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                    <div class="pro_two">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                </div>
                                <div class="product-icon">
                                    <a href="#"><i class="fa fa-shopping-cart"> </i></a>
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                    <a href="#"><i class="fa fa-refresh"> </i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" col-md-3">
                        <div class="single-product">
                            <span class="sale-text">Sale</span>
                            <div class="product-img">
                                <a href="#">
                                    <img class="primary-image" src="img/product/1_1.jpg" alt="" />
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="price-box">
                                    <span class="special-price">$155.00</span>
                                </div>
                                <h2 class="product-name"><a href="#">Cras neque metus</a></h2>
                                <div class="pro-rating">
                                    <div class="pro_one">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                    <div class="pro_two">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                </div>
                                <div class="product-icon">
                                    <a href="#"><i class="fa fa-shopping-cart"> </i></a>
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                    <a href="#"><i class="fa fa-refresh"> </i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" col-md-3">
                        <div class="single-product">
                            <span class="sale-text">Sale</span>
                            <div class="product-img">
                                <a href="#">
                                    <img class="primary-image" src="img/product/12_2.jpg" alt="" />
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="price-box">
                                    <span class="special-price">$222.00</span>
                                </div>
                                <h2 class="product-name"><a href="#">Nunc facilisis</a></h2>
                                <div class="pro-rating">
                                    <div class="pro_one">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                    <div class="pro_two">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                </div>
                                <div class="product-icon">
                                    <a href="#"><i class="fa fa-shopping-cart"> </i></a>
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                    <a href="#"><i class="fa fa-refresh"> </i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" col-md-3">
                        <div class="single-product">
                            <span class="sale-text">Sale</span>
                            <div class="product-img">
                                <a href="#">
                                    <img class="primary-image" src="img/product/15_1.jpg" alt="" />
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="price-box">
                                    <span class="special-price">$99.00</span>
                                    <span class="old-price">$111.00</span>
                                </div>
                                <h2 class="product-name"><a href="#">Primis in faucibus</a></h2>
                                <div class="pro-rating">
                                    <div class="pro_one">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                    <div class="pro_two">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                </div>
                                <div class="product-icon">
                                    <a href="#"><i class="fa fa-shopping-cart"> </i></a>
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                    <a href="#"><i class="fa fa-refresh"> </i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" col-md-3">
                        <div class="single-product">
                            <span class="sale-text">Sale</span>
                            <div class="product-img">
                                <a href="#">
                                    <img class="primary-image" src="img/product/17.jpg" alt="" />
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="price-box">
                                    <span class="special-price">$333.00</span>
                                </div>
                                <h2 class="product-name"><a href="#">Accumsan elit </a></h2>
                                <div class="pro-rating">
                                    <div class="pro_one">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                    <div class="pro_two">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                </div>
                                <div class="product-icon">
                                    <a href="#"><i class="fa fa-shopping-cart"> </i></a>
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                    <a href="#"><i class="fa fa-refresh"> </i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" col-md-3">
                        <div class="single-product">
                            <span class="sale-text">Sale</span>
                            <div class="product-img">
                                <a href="#">
                                    <img class="primary-image" src="img/product/18.jpg" alt="" />
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="price-box">
                                    <span class="special-price">$345.00</span>
                                </div>
                                <h2 class="product-name"><a href="#">occaecati cupiditate</a></h2>
                                <div class="pro-rating">
                                    <div class="pro_one">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                    <div class="pro_two">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                </div>
                                <div class="product-icon">
                                    <a href="#"><i class="fa fa-shopping-cart"> </i></a>
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                    <a href="#"><i class="fa fa-refresh"> </i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" col-md-3">
                        <div class="single-product">
                            <span class="sale-text">Sale</span>
                            <div class="product-img">
                                <a href="#">
                                    <img class="primary-image" src="img/product/16_3.jpg" alt="" />
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="price-box">
                                    <span class="special-price">$211.00</span>
                                </div>
                                <h2 class="product-name"><a href="#">consequences</a></h2>
                                <div class="pro-rating">
                                    <div class="pro_one">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                    <div class="pro_two">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                </div>
                                <div class="product-icon">
                                    <a href="#"><i class="fa fa-shopping-cart"> </i></a>
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                    <a href="#"><i class="fa fa-refresh"> </i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" col-md-3">
                        <div class="single-product">
                            <span class="sale-text">Sale</span>
                            <div class="product-img">
                                <a href="#">
                                    <img class="primary-image" src="img/product/20.jpg" alt="" />
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="price-box">
                                    <span class="special-price">$222.00</span>
                                    <span class="special-price">$333.00</span>
                                </div>
                                <h2 class="product-name"><a href="#">pleasure rationally</a></h2>
                                <div class="pro-rating">
                                    <div class="pro_one">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                    <div class="pro_two">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
                                </div>
                                <div class="product-icon">
                                    <a href="#"><i class="fa fa-shopping-cart"> </i></a>
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                    <a href="#"><i class="fa fa-refresh"> </i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single-product end -->
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- start shop_area
============================================ -->
<section class="shop_area">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="title ma-title lab">
                <h2>
                Our brands
                </h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="item_all indicator-style item-down">
            <div class="col-md-12">
                <img class="primary-img" src="img/item/br1.jpg" alt="" />
            </div>
            <div class="col-md-12">
                <img class="primary-img" src="img/item/br1.jpg" alt="" />
            </div>
            <div class="col-md-12">
                <img class="primary-img" src="img/item/br1.jpg" alt="" />
            </div>
            <div class="col-md-12">
                <img class="primary-img" src="img/item/br1.jpg" alt="" />
            </div>
            <div class="col-md-12">
                <img class="primary-img" src="img/item/br1.jpg" alt="" />
            </div>
            <div class="col-md-12">
                <img class="primary-img" src="img/item/br1.jpg" alt="" />
            </div>
            <div class="col-md-12">
                <img class="primary-img" src="img/item/br1.jpg" alt="" />
            </div>
            <div class="col-md-12">
                <img class="primary-img" src="img/item/br1.jpg" alt="" />
            </div>
            <div class="col-md-12">
                <img class="primary-img" src="img/item/br1.jpg" alt="" />
            </div>
        </div>
    </div>
</div>
</section>