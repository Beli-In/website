<section class="main_contact_area">
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="container-inner">
                        <ul>
                            <li class="home">
                                <a href="#">Home</a>
                                <span>
                                    <i class="fa fa-angle-right"></i>
                                </span>
                            </li>
                            <li class="category3">
                                <strong>contact </strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row contact-content-area">
                <h2>Customer service -<strong> Contact us</strong></h2>
                <div class="contact-baner">
                    <div class="contact-text">
                        <h2>Contact info</h2>
                        <p>get in touch</p>
                    </div>
                </div>
            </div>
            <?php if (!is_null($maps_key)): ?>
<div class="row contact-map">
                <h3>We are located</h3>
                <div id="hastech"></div>
            </div>
            <?php endif; ?>
            


            <div class="contact-from-atea">
                <div class="form-and-info">
                    <div class="col-sm-5 col-md-4 npl">
                        <div class="contactDetails contactHead">
                            <h3>CONTACT Info</h3>
                            <p>
                                <span class="iconContact">
                                    <i class="fa fa-map-marker"></i>
                                </span>
                                <?php echo $this->Options->get('address'); ?>
                            </p>
                            <p>
                                <span class="iconContact">
                                    <i class="fa fa-phone"></i>
                                </span>
                                Telephone: <?php echo $this->Options->get('telp'); ?>
                                <br>
                            </p>
                            <p>
                                <span class="iconContact">
                                    <i class="fa fa-envelope-o"></i>
                                </span>
                                Email: <?php echo $this->Options->get('email'); ?>
                                <br>
                            </p>
                        </div>
                        <div class="social-area contactHead">
                            <h3>Follow us</h3>
                            <ul class="socila-icon">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-rss"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-7 col-md-8 npr">
                        <div class="contactfrom">
                            <h1>message</h1>
                            <form class="">
                                <div class="col-md-6 npl">
                                    <input id="InputName" class="form-control" type="text" placeholder="Full name" required="">
                                </div>
                                <div class="col-md-6 contactemail npr">
                                    <input id="InputEmail" class="form-control" type="email" placeholder="Email" required="">
                                </div>
                                <div class="col-md-12 np">
                                    <textarea class="form-control" rows="13" placeholder="Massage" required=""></textarea>
                                </div>
                                <button class="btn btnContact" type="submit">send message</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>