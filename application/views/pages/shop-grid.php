<!-- Start breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="container-inner">
			<ul>
				<li class="home">
					<a title="Go to Home Page" href="#">Home</a><span><i class="fa fa-angle-double-right"></i></span></li>
				<li class="category8"><strong>All Products</strong></li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->
<!-- Start product grid view -->
<section class="product-grid-view">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-12" style="padding-top: 23px;">
				<div class="product-view-sidebar">
					<div class="block block-layered-nav">
						<div class="block-title"><strong><span>Shop By</span></strong></div>
						<div class="block-content">
							<div id="narrow-by-list">
								<div class="layered-attribute">
									<div class="title-layered"><span>Category</span></div>
									<div class="odd">
										<ul>
										    <li><a href="#">Cocktail</a><span> (2) </span></li>
										    <li><a href="#">Day</a><span> (2) </span></li>
										    <li><a href="#">Evening</a><span> (2) </span></li>
										    <li><a href="#">Sports</a><span> (2) </span></li>
										</ul>
									</div>
								</div>
								<div class="layered-price">
									<div class="title-layered"><span>Price</span></div> 
									<input type="text" disabled ="disabled" id="amount" style="" />
								    <div id ="search1" style ="margin:10px 10px 0px 10px;">
										<div id="slider-range"></div>
								        <button class="button" type ="button" name ="search_price" id ="search_price">
								        	<span><span>Show</span></span>
								        </button>
								    </div>
								</div>
								<div class="layered-attribute">
									<div class="title-layered"><span>Manufacturer</span></div>
									<div class="odd">
										<ul>
										    <li><a href="#">Chanel</a><span> (1) </span></li>
										    <li><a href="#">Dolce</a><span> (1) </span></li>
										</ul>
									</div>
									<br>
								</div>
							</div>
							<br>
							<div class="hidden-xs banner-left">
								<a href="#">
									<img src="<?php echo assets('img/single-product/banner-left.png'); ?>" alt="">
								</a>
							</div>
							<div class="block block-tags">
							    <div class="block-title">
							        <strong><span>Popular Tags</span></strong>
							    </div>
								<div class="block-content">
							        <ul class="tags-list">
							            <li><a href="#">Clothing</a></li>
							            <li><a href="#">accessories</a></li>
							            <li><a href="#">fashion</a></li>
							            <li><a href="#">footwear</a></li>
							            <li><a href="#">good</a></li>
							            <li><a href="#">kid</a></li>
							            <li><a href="#">men</a></li>
							            <li><a href="#">women</a></li>
							        </ul>
							        <div class="actions">
							            <a href="#">View All Tags</a>
							        </div>
							    </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-9 col-sm-12">
				<!-- Start categori content -->
                <div id="content-shop" class="category-products">
                    <!-- Start catagori short -->
                    <div class="toolbar">
						<div class="sorter">
						    <div class="sort-by hidden-xs">
						        <label>Sort By</label>
						        <select>
						            <option>Position</option>
						            <option>Name</option>
						            <option>Price</option>
						        </select>
						        <a title="Set Descending Direction" href="#"><i class="fa fa-long-arrow-up"></i></a>
						    </div>
						</div>
                    </div>
                    <!-- End catagori short -->
                        <!-- Start categori grid view -->
                    <div id="grid" class="tab-pane active categoti-grid-view row">
                        <div class="single-product-tab">
							<!-- Start single product -->
							<div class="col-sm-4">
								<div class="item-inner">
									<div class="ma-box-content">
										<div class="products">
											<a class="product-image" title="Fusce aliquam" href="single-product.html">
												<span class="product-image">
													<img alt="Fusce aliquam" src="<?php echo assets('img/product/03.jpg'); ?>">
												</span>
												<span class="product-image image-rotator">
													<img alt="Fusce aliquam" src="<?php echo assets('img/product/05.jpg'); ?>">
												</span>   
											</a>
											<div class="ma-buttons">
												<ul class="add-to-links">
													<li>
														<a class="link-wishlist fa fa-heart" href="#">
															<em>Add to Wishlist</em>
														</a>
													</li>			
													<li>
														<span class="separator">|</span> 
														<a class="link-compare fa fa-refresh" href="#">
															<em>Add to Compare</em>
														</a>
													</li>
												</ul>
											</div> 	 
										</div>
										<div class="box-desc">
											<div class="ratings">
							                    <div class="rating-box"><div class="rating"></div></div>
							                	<span class="amount"><a href="#">1 Review(s)</a></span>
			    							</div>							
											<h2 class="product-name"><a title="Fusce aliquam" href="#">Fusce aliquam</a></h2>
											<div class="price-box">
												<p class="old-price"><span class="price-label">Regular Price: </span><span class="price">$170.00</span></p>
												<p class="special-price"><span class="price-label">Special Price</span><span class="price">$155.00</span>
												</p> 
											</div>
											<div class="actions clearfix">
												<a href="cart.html">Add to Cart</a>		
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- End single product -->
							<!-- Start single product -->
							<div class="col-sm-4">
								<div class="item-inner">
									<div class="ma-box-content">
										<div class="products">
											<a class="product-image" title="Fusce aliquam" href="single-product.html">
												<span class="product-image">
													<img alt="Fusce aliquam" src="<?php echo assets('img/product/117.jpg'); ?>">
												</span>
												<span class="product-image image-rotator">
													<img alt="Fusce aliquam" src="<?php echo assets('img/product/119.jpg'); ?>">
												</span>   
											</a>
											<div class="ma-buttons">
												<ul class="add-to-links">
													<li>
														<a class="link-wishlist fa fa-heart" href="#">
															<em>Add to Wishlist</em>
														</a>
													</li>			
													<li>
														<span class="separator">|</span> 
														<a class="link-compare fa fa-refresh" href="#">
															<em>Add to Compare</em>
														</a>
													</li>
												</ul>
											</div> 	 
										</div>
										<div class="box-desc">
											<div class="ratings">
							                    <div class="rating-box"><div class="rating"></div></div>
							                	<span class="amount"><a href="#">1 Review(s)</a></span>
			    							</div>							
											<h2 class="product-name"><a title="Fusce aliquam" href="#">Donec ac tempus</a></h2>
											<div class="price-box">
												<p class="special-price"><span class="price-label">Special Price</span><span class="price">$222.00</span>
												</p> 
											</div>
											<div class="actions clearfix">
												<a href="cart.html">Add to Cart</a>		
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- End single product -->
							<!-- Start single product -->
							<div class="col-sm-4">
								<div class="item-inner">
									<div class="ma-box-content">
										<div class="products">
											<a class="product-image" title="Fusce aliquam" href="single-product.html">
												<span class="product-image">
													<img alt="Fusce aliquam" src="<?php echo assets('img/product/02_1.jpg'); ?>">
												</span>
												<span class="product-image image-rotator">
													<img alt="Fusce aliquam" src="<?php echo assets('img/product/10_1.jpg'); ?>">
												</span>   
											</a>
											<div class="ma-buttons">
												<ul class="add-to-links">
													<li>
														<a class="link-wishlist fa fa-heart" href="#">
															<em>Add to Wishlist</em>
														</a>
													</li>			
													<li>
														<span class="separator">|</span> 
														<a class="link-compare fa fa-refresh" href="#">
															<em>Add to Compare</em>
														</a>
													</li>
												</ul>
											</div> 	 
										</div>
										<div class="box-desc">
											<div class="ratings">
							                    <div class="rating-box"><div class="rating"></div></div>
							                	<span class="amount"><a href="#">1 Review(s)</a></span>
			    							</div>							
											<h2 class="product-name"><a title="Fusce aliquam" href="#">Pellentesque habitant</a></h2>
											<div class="price-box">
												<p class="old-price"><span class="price-label">Regular Price: </span><span class="price">$333.00</span></p>
												<p class="special-price"><span class="price-label">Special Price</span><span class="price">$320.00</span>
												</p> 
											</div>
											<div class="actions clearfix">
												<a href="cart.html">Add to Cart</a>		
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- End single product -->
							<!-- Start single product -->
							<div class="col-sm-4">
								<div class="item-inner">
									<div class="ma-box-content">
										<div class="products">
											<a class="product-image" title="Fusce aliquam" href="single-product.html">
												<span class="product-image">
													<img alt="Fusce aliquam" src="<?php echo assets('img/product/05.jpg'); ?>">
												</span>
												<span class="product-image image-rotator">
													<img alt="Fusce aliquam" src="<?php echo assets('img/product/03.jpg'); ?>">
												</span>   
											</a>
											<div class="ma-buttons">
												<ul class="add-to-links">
													<li>
														<a class="link-wishlist fa fa-heart" href="#">
															<em>Add to Wishlist</em>
														</a>
													</li>			
													<li>
														<span class="separator">|</span> 
														<a class="link-compare fa fa-refresh" href="#">
															<em>Add to Compare</em>
														</a>
													</li>
												</ul>
											</div> 	 
										</div>
										<div class="box-desc">
											<div class="ratings">
							                    <div class="rating-box"><div class="rating"></div></div>
							                	<span class="amount"><a href="#">1 Review(s)</a></span>
			    							</div>							
											<h2 class="product-name"><a title="Fusce aliquam" href="#">Fusce aliquam</a></h2>
											<div class="price-box">
												<p class="old-price"><span class="price-label">Regular Price: </span><span class="price">$170.00</span></p>
												<p class="special-price"><span class="price-label">Special Price</span><span class="price">$155.00</span>
												</p> 
											</div>
											<div class="actions clearfix">
												<a href="cart.html">Add to Cart</a>		
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- End single product -->
							<!-- Start single product -->
							<div class="col-sm-4">
								<div class="item-inner">
									<div class="ma-box-content">
										<div class="products">
											<a class="product-image" title="Fusce aliquam" href="single-product.html">
												<span class="product-image">
													<img alt="Fusce aliquam" src="<?php echo assets('img/product/03_1_1.jpg'); ?>">
												</span>
												<span class="product-image image-rotator">
													<img alt="Fusce aliquam" src="<?php echo assets('img/product/04_1_1.jpg'); ?>">
												</span>   
											</a>
											<div class="ma-buttons">
												<ul class="add-to-links">
													<li>
														<a class="link-wishlist fa fa-heart" href="#">
															<em>Add to Wishlist</em>
														</a>
													</li>			
													<li>
														<span class="separator">|</span> 
														<a class="link-compare fa fa-refresh" href="#">
															<em>Add to Compare</em>
														</a>
													</li>
												</ul>
											</div> 	 
										</div>
										<div class="box-desc">
											<div class="ratings">
							                    <div class="rating-box"><div class="rating"></div></div>
							                	<span class="amount"><a href="#">1 Review(s)</a></span>
			    							</div>							
											<h2 class="product-name"><a title="Fusce aliquam" href="#">Etiam gravida</a></h2>
											<div class="price-box">
												<p class="special-price"><span class="price-label">Special Price</span><span class="price">$432.00</span>
												</p> 
											</div>
											<div class="actions clearfix">
												<a href="cart.html">Add to Cart</a>		
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- End single product -->
							<!-- Start single product -->
							<div class="col-sm-4">
								<div class="item-inner">
									<div class="ma-box-content">
										<div class="products">
											<a class="product-image" title="Fusce aliquam" href="single-product.html">
												<span class="product-image">
													<img alt="Fusce aliquam" src="<?php echo assets('img/product/08.jpg'); ?>">
												</span>
												<span class="product-image image-rotator">
													<img alt="Fusce aliquam" src="<?php echo assets('img/product/10.jpg'); ?>">
												</span>   
											</a>
											<div class="ma-buttons">
												<ul class="add-to-links">
													<li>
														<a class="link-wishlist fa fa-heart" href="#">
															<em>Add to Wishlist</em>
														</a>
													</li>			
													<li>
														<span class="separator">|</span> 
														<a class="link-compare fa fa-refresh" href="#">
															<em>Add to Compare</em>
														</a>
													</li>
												</ul>
											</div> 	 
										</div>
										<div class="box-desc">
											<div class="ratings">
							                    <div class="rating-box"><div class="rating"></div></div>
							                	<span class="amount"><a href="#">1 Review(s)</a></span>
			    							</div>							
											<h2 class="product-name"><a title="Fusce aliquam" href="#">Proin lectus ipsum</a></h2>
											<div class="price-box">
												<p class="old-price"><span class="price-label">Regular Price: </span><span class="price">$99.00</span></p>
												<p class="special-price"><span class="price-label">Special Price</span><span class="price">$88.00</span>
												</p> 
											</div>
											<div class="actions clearfix">
												<a href="cart.html">Add to Cart</a>		
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- End single product -->
							<!-- Start single product -->
							<div class="col-sm-4">
								<div class="item-inner">
									<div class="ma-box-content">
										<div class="products">
											<a class="product-image" title="Fusce aliquam" href="single-product.html">
												<span class="product-image">
													<img alt="Fusce aliquam" src="<?php echo assets('img/product/11_1.jpg'); ?>">
												</span>
												<span class="product-image image-rotator">
													<img alt="Fusce aliquam" src="<?php echo assets('img/product/12.jpg'); ?>">
												</span>   
											</a>
											<div class="ma-buttons">
												<ul class="add-to-links">
													<li>
														<a class="link-wishlist fa fa-heart" href="#">
															<em>Add to Wishlist</em>
														</a>
													</li>			
													<li>
														<span class="separator">|</span> 
														<a class="link-compare fa fa-refresh" href="#">
															<em>Add to Compare</em>
														</a>
													</li>
												</ul>
											</div> 	 
										</div>
										<div class="box-desc">
											<div class="ratings">
							                    <div class="rating-box"><div class="rating"></div></div>
							                	<span class="amount"><a href="#">1 Review(s)</a></span>
			    							</div>							
											<h2 class="product-name"><a title="Fusce aliquam" href="#">Aliquam consequat</a></h2>
											<div class="price-box">
												<p class="special-price"><span class="price-label">Special Price</span><span class="price">$123.00</span>
												</p> 
											</div>
											<div class="actions clearfix">
												<a href="cart.html">Add to Cart</a>		
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- End single product -->
							<!-- Start single product -->
							<div class="col-sm-4">
								<div class="item-inner">
									<div class="ma-box-content">
										<div class="products">
											<a class="product-image" title="Fusce aliquam" href="single-product.html">
												<span class="product-image">
													<img alt="Fusce aliquam" src="<?php echo assets('img/product/11_1.jpg'); ?>">
												</span>
												<span class="product-image image-rotator">
													<img alt="Fusce aliquam" src="<?php echo assets('img/product/12.jpg'); ?>">
												</span>   
											</a>
											<div class="ma-buttons">
												<ul class="add-to-links">
													<li>
														<a class="link-wishlist fa fa-heart" href="#">
															<em>Add to Wishlist</em>
														</a>
													</li>			
													<li>
														<span class="separator">|</span> 
														<a class="link-compare fa fa-refresh" href="#">
															<em>Add to Compare</em>
														</a>
													</li>
												</ul>
											</div> 	 
										</div>
										<div class="box-desc">
											<div class="ratings">
							                    <div class="rating-box"><div class="rating"></div></div>
							                	<span class="amount"><a href="#">1 Review(s)</a></span>
			    							</div>							
											<h2 class="product-name"><a title="Fusce aliquam" href="#">Aliquam consequat</a></h2>
											<div class="price-box">
												<p class="special-price"><span class="price-label">Special Price</span><span class="price">$123.00</span>
												</p> 
											</div>
											<div class="actions clearfix">
												<a href="cart.html">Add to Cart</a>		
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- End single product -->
							<!-- Start single product -->
							<div class="col-sm-4">
								<div class="item-inner">
									<div class="ma-box-content">
										<div class="products">
											<a class="product-image" title="Fusce aliquam" href="single-product.html">
												<span class="product-image">
													<img alt="Fusce aliquam" src="<?php echo assets('img/product/11_1.jpg'); ?>">
												</span>
												<span class="product-image image-rotator">
													<img alt="Fusce aliquam" src="<?php echo assets('img/product/12.jpg'); ?>">
												</span>   
											</a>
											<div class="ma-buttons">
												<ul class="add-to-links">
													<li>
														<a class="link-wishlist fa fa-heart" href="#">
															<em>Add to Wishlist</em>
														</a>
													</li>			
													<li>
														<span class="separator">|</span> 
														<a class="link-compare fa fa-refresh" href="#">
															<em>Add to Compare</em>
														</a>
													</li>
												</ul>
											</div> 	 
										</div>
										<div class="box-desc">
											<div class="ratings">
							                    <div class="rating-box"><div class="rating"></div></div>
							                	<span class="amount"><a href="#">1 Review(s)</a></span>
			    							</div>							
											<h2 class="product-name"><a title="Fusce aliquam" href="#">Aliquam consequat</a></h2>
											<div class="price-box">
												<p class="special-price"><span class="price-label">Special Price</span><span class="price">$123.00</span>
												</p> 
											</div>
											<div class="actions clearfix">
												<a href="cart.html">Add to Cart</a>		
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- End single product -->
						</div>
                        <!-- End categori grid view -->
                    <!-- Start catagori short -->
                    <div class="toolbar">
						<div class="sorter">
						    <div class="sort-by hidden-xs">
						        <label>Sort By</label>
						        <select>
						            <option>Position</option>
						            <option>Name</option>
						            <option>Price</option>
						        </select>
						        <a title="Set Descending Direction" href="#"><i class="fa fa-long-arrow-up"></i></a>
						    </div>
						</div>
						<div class="pager">
							<div class="view-mode">
								<label>View as:</label>
								<ul id="gridlist2" class="nav nav-tabs" data-tabs="tabs">
						            <li class="active"><a href="#grid" data-toggle="tab"><i class="fa fa-th-large"></i></a></li>
						            <li><a href="#list" data-toggle="tab"><i class="fa fa-th-list"></i></a></li>
						        </ul>
							</div>
						</div>
                    </div>
                    <!-- End catagori short -->
                </div>
                <!-- Start categori content -->
			</div>
		</div>
	</div>
</section>
<!-- End product grid view -->