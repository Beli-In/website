<div class="wishlists-page-content">
	<div class="container">
	    <div class="row">
	        <div class="col-sm-3">
	        	<div class="block block-account">
				    <div class="block-title">
				        <strong><span>My Account</span></strong>
				    </div>
				    <div class="block-content">
				        <ul>
					        <li class="current"><a href="<?php echo base_url('account/info'); ?>"><strong>Account Dashboard</strong></a></li>
					        <li><a href="<?php echo base_url('account/address'); ?>">Address Book</a></li>
					        <li><a href="<?php echo base_url('account/history'); ?>">My Orders</a></li>
					        <li><a href="<?php echo base_url('account/review'); ?>">My Product Reviews</a></li>
					        <li><a href="#">Accounts Settings</a></li>
					    </ul>
				    </div>
				</div>
	        </div>
	        <div class="col-sm-9">
	        	<div class="my-account">
	        		<div class="dashboard">
					    <div class="page-title">
					        <h1>My Dashboard</h1>
					    </div>
					    <div class="welcome-msg">
						    <p class="hello"><strong>Hello, <?php echo $user->full_name; ?>!</strong></p>
						    <p>From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information. Select a link below to view or edit information.</p>
						</div>
					    <div class="box-account box-info">
					        <div class="box-head">
					            <h2>Account Information</h2>
					        </div>
						    <div class="col2-set row">
							    <div class="col-1 col-sm-6 np">
							        <div class="box">
							            <div class="box-title">
							                <h3>Contact Information</h3>
							            </div>
							            <div class="box-content">
							                <p>
							                    <?php echo $user->full_name; ?><br>
							                    <?php echo $user->email; ?><br>
							                    <?php echo $user->phone; ?><br>
							                    <a href="<?php echo base_url('account/change_password') ?>">Ganti Password</a>
							                </p>
							            </div>
							        </div>
							    </div>
						        <div class="col-2 col-sm-6 np">
							        <div class="box">
							            <div class="box-title">
							                <h3>Newsletters</h3>
							            </div>
							            <div class="box-content">
							                <p> You are currently subscribed to 'General Subscription'.</p>
							            </div>
							        </div>
						        </div>
						    </div>
					        <div class="col2-set">
							    <div class="box row">
							        	
							    </div>
							</div>
					    </div>
			        </div>
			    </div>
	        </div>
	    </div>
	</div>
</div>