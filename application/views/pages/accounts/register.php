<div class="wishlists-page-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
            	<div class="container-inner">
            		<div class="account-create">
            			<div class="page-title">
            				<h1>Create an Account</h1>
            			</div>
            			<?php echo form_open(current_url(), ['id' => 'registerform']); ?>	
            			<?php if (!empty(validation_errors())): ?>
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <?php echo validation_errors(); ?>
                            </div>
                        <?php endif ?>
                        <?php if (getFlash('notif')!==false): ?>
                            <div class="alert alert-<?php echo getFlash('type'); ?>">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            						<?php echo getFlash('notif', true); ?>
                            </div>
                        <?php endif ?>
						<div class="fieldset">
				            <h2 class="legend">Personal Information</h2>
				            <ul class="form-list">
				                 <div class="row">
									<div class="col-md-6">
					                	<div class="form-group">
					                		<label for="nama" class="col-sm-3">Nama Lengkap : </label>
											<input type="text" name="nama" id="inputName" class="form-control" value="<?php echo set_value('nama	'); ?>" required="required" placeholder="Masukan Nama Lengkap Asli Anda">
				                		</div>
					                </div>
				                	<div class="col-md-6">
						                <div class="form-group">
						                	<label for="birthday" class="col-sm-3">No Handphone</label>
						                	<input type="text" name="phone" class="form-control" value="<?php echo set_value('phone'); ?>" required="required" title="" placeholder="Nomor Handphone Aktif Anda">
						                </div>
					                </div>
				                </div>
				                <div class="row">
				                	<div class="col-md-6">
						                <div class="form-group">
						                	<label for="birthday" class="col-sm-3">Tanggal Lahir</label>
						                	<input type="text" name="birthday" id="date" class="form-control" value="<?php echo set_value('birthday'); ?>" required="required" title="" placeholder="Pilih Tanggal Lahir Anda">
						                </div>
					                </div>
					                <div class="col-md-6">
						                <div class="form-group">
						                	<label for="email" class="col-sm-3">Jenis Kelamin</label>
						                 	<select name="gender" id="inputGender" class="form-control" required="required">
						                 		<option value="">== Pilih Jenis Kelamin ==</option>
						                 		<option value="male">Laki - Laki</option>
						                 		<option value="female">Perempuan</option>
						                 	</select>
						                </div>
					                </div>
				                </div>
				            </ul>
				        </div>
				        <div class="fieldset">
				            <h2 class="legend">Login Information</h2>
				            <ul class="form-list">
				            	<div class="row">
				                	<div class="col-md-6">
						                <div class="form-group">
						                	<label for="email" class="col-sm-3">E-mail</label>
						                	<input type="email" name="useremail" id="inputEmail" class="form-control" value="<?php echo set_value('useremail'); ?>" required="required" title="" placeholder="Masukan E-Mail Aktif Anda">
						                </div>
					                </div>
					                <div class="col-md-6">
						                <div class="form-group">
						                	<label for="email" class="col-sm-3">Confirm E-mail</label>
						                	<input type="email" name="confemail" id="inputEmail" class="form-control" required="required" title="" placeholder="Masukan E-Mail Aktif Anda Lagi">
						                </div>
					                </div>
				                </div>
				                <div class="row">
				                	<div class="col-md-6">
						                <div class="form-group">
						                	<label for="email" class="col-sm-3">Password</label>
						                	<input type="password" name="password" class="form-control" required="required" title="" placeholder="Masukan Password Baru Anda">
						                </div>
					                </div>
					                <div class="col-md-6">
						                <div class="form-group">
						                	<label for="email" class="col-sm-3">Confirm Password</label>
						                	<input type="password" name="confpassword" class="form-control" required="required" title="" placeholder="Masukan Password Baru Anda Lagi">
						                </div>
					                </div>
				                </div>
				            </ul>
				        </div>

				        <div class="fieldset">
				            <h2 class="legend">Human Proof</h2>
				            <ul class="form-list">
				            	<div class="row">
				                	<div class="col-md-12">
						                <?php echo recaptcha_open(); ?>
					                </div>
				                </div>
				            </ul>
				        </div>
				        <div class="buttons-set">
				            <p class="back-link"><a class="back-link" href="#"><small>« </small>Back</a></p>
				            <button class="button" title="Submit" type="submit"><span><span>Submit</span></span></button>
				        </div>
            		</div>
            	</div>
            </div>
        </div>
    </div>
</div>