<div class="wishlists-page-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 np">
            	<div id="checkoutMethod" class="panel-collapse collapse in">
                    <div class="content-info">
                        <div class="col-md-6">
                            <div class="checkout-reg">
                                <div class="checkReg commonChack">
                                    <div class="checkTitle">
                                        <h6 class="ct-design">New Customers</h6>
                                    </div>
                                </div>
                                <div class="regSaveTime commonChack">
                                    <h6>REGISTER AND SAVE TIME!</h6>
                                    <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                                </div>
                                <a href="<?php echo base_url('register'); ?>" class="create-account checkPageBtn pull-right">Create an Account</a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="checkout-login">
                                    <div class="checkTitle">
                                        <h6 class="ct-design">Login</h6>
                                    </div>
                                <p class="alrdyReg">ALREADY REGISTERED?</p>
                                <?php if (!empty(validation_errors())): ?>
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <?php echo validation_errors(); ?>
                                    </div>
                                <?php endif ?>
                                <?php flashmassage() ?>
                                <p class="plxLogin">Please log in below:</p>
                                <?php echo form_open(); ?>
                                <div class="loginFrom">
                                    <p class="plxLoginP">Email Address</p>
                                    <input type="email" name="email" id="inputEmail" class="form-control" value="<?php echo set_value('email'); ?>" required="required" title="Email Field" placeholder="Masukan Email Account Anda">
                                    <p class="plxLoginP">Password</p>
                                    <input type="password" name="password" id="inputPassword" class="form-control" required="required" title="Password Field" placeholder="Masukan Password Account Anda">
                                            <input type="checkbox" value="rememberme">
                                            Remember me
                                            <br/><br/>
                                    <p class="plxLoginP">Captcha</p>
                                    <?php echo recaptcha_open(); ?>
                                    <p class="rqudField">* Required Fields</p>
                                    <p class="plxLogin">Forgot your password ?</p>
                                </div>
                                <button href="#" class="checkPageBtn pull-right">Login</button>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>