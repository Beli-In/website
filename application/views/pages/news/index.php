<div class="main_contact_area">
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="container-inner">
                        <ul>
                            <li class="home">
                                <a href="#">Home</a>
                                <span>
                                    <i class="fa fa-angle-right"></i>
                                </span>
                            </li>
                            <li class="category3">
                                <strong>Blog</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>
<section class="main-blog-area">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="single-bolg">
                    <div class="post-format-area">
                        <div class="b-slide-all">
                            <img src="<?php echo assets('img/blog/1.jpg'); ?>" alt="">
                            <img src="<?php echo assets('img/blog/2.jpg'); ?>" alt="">
                            <img src="<?php echo assets('img/blog/3.jpg'); ?>" alt="">
                        </div>
                    </div>
                    <div class="entry-header-area">
                        <div class="post-types">
                            <i class="fa fa-picture-o"></i>
                        </div>
                        <div class="info-blog">
                            <div class="single-b-info category-name">
                                <i class="fa fa-folder-open-o"></i>
                                <a href="#">
                                    <span>Blog</span>
                                </a>
                            </div>
                            <div class="single-b-info createdby">
                                <i class="fa fa-user"></i>
                                <span>Super User</span>
                            </div>
                            <div class="single-b-info hits">
                                <i class="fa fa-eye"></i>
                                <span>Hits: 256</span>
                            </div>
                            <div class="single-b-info post_rating">
                                Rating:
                                <div class="voting-symbol">
                                    <span class="star active"></span>
                                    <span class="star active"></span>
                                    <span class="star active"></span>
                                    <span class="star"></span>
                                    <span class="star"></span>
                                </div>
                            </div>
                            <h2 class="name">
                                <a href="#">This is images</a>
                            </h2>
                        </div>
                    </div>
                    <p>Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus. Praesent ornare tortor ac ante egestas hendrerit.</p>
                    <div class="blog-comments-links">
                        <a class="readmore-link" href="#" title="Images">Read more ...</a>
                        <a class="comments-link" href="#" title="1 comment">1 comment</a>
                    </div>
                </div>
                <div class="single-bolg">
                    <div class="post-format-area">
                        <div class="blog_video embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/GHIsGTWeeQ4" allowfullscreen="">
                            </iframe>
                        </div>
                    </div>
                    <div class="entry-header-area">
                        <div class="post-types">
                            <i class="fa fa-video-camera"></i>
                        </div>
                        <div class="info-blog">
                            <div class="single-b-info category-name">
                                <i class="fa fa-folder-open-o"></i>
                                <a href="#">
                                    <span>Video</span>
                                </a>
                            </div>
                            <div class="single-b-info createdby">
                                <i class="fa fa-user"></i>
                                <span>Tayeb Rayed</span>
                            </div>
                            <div class="single-b-info hits">
                                <i class="fa fa-eye"></i>
                                <span>Hits: 256</span>
                            </div>
                            <div class="single-b-info post_rating">
                                Rating:
                                <div class="voting-symbol">
                                    <span class="star active"></span>
                                    <span class="star active"></span>
                                    <span class="star active"></span>
                                    <span class="star"></span>
                                    <span class="star"></span>
                                </div>
                            </div>
                            <h2 class="name">
                                <a href="#">This is video</a>
                            </h2>
                        </div>
                    </div>
                    <p>Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus. Praesent ornare tortor ac ante egestas hendrerit.</p>
                    <div class="blog-comments-links">
                        <a class="readmore-link" href="#" title="Images">Read more ...</a>
                        <a class="comments-link" href="#" title="1 comment">1 comment</a>
                    </div>
                </div>
                <div class="single-bolg">
                    <div class="post-format-area">
                        <div class="player">
                            <audio class="audio-format" controls="">
                                <source type="audio/ogg" src="media/Kalimba.mp3">
                            </audio>
                        </div>
                    </div>
                    <div class="entry-header-area">
                        <div class="post-types">
                            <i class="fa fa-music"></i>
                        </div>
                        <div class="info-blog">
                            <div class="single-b-info category-name">
                                <i class="fa fa-folder-open-o"></i>
                                <a href="#">
                                    <span>Music</span>
                                </a>
                            </div>
                            <div class="single-b-info createdby">
                                <i class="fa fa-user"></i>
                                <span>Tayeb Rayed</span>
                            </div>
                            <div class="single-b-info hits">
                                <i class="fa fa-eye"></i>
                                <span>Hits: 256</span>
                            </div>
                            <div class="single-b-info post_rating">
                                Rating:
                                <div class="voting-symbol">
                                    <span class="star active"></span>
                                    <span class="star active"></span>
                                    <span class="star active"></span>
                                    <span class="star"></span>
                                    <span class="star"></span>
                                </div>
                            </div>
                            <h2 class="name">
                                <a href="#">I love this music so much</a>
                            </h2>
                        </div>
                    </div>
                    <p>Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus. Praesent ornare tortor ac ante egestas hendrerit.</p>
                    <div class="blog-comments-links">
                        <a class="readmore-link" href="#" title="Images">Read more ...</a>
                        <a class="comments-link" href="#" title="1 comment">1 comment</a>
                    </div>
                </div>
                <div class="single-bolg blog">
                    <div class="post-format-area vmio-v embed-responsive embed-responsive-16by9">
                        <iframe allowfullscreen="" src="https://player.vimeo.com/video/43426940">
                        </iframe>
                    </div>
                    <div class="entry-header-area">
                        <div class="post-types">
                            <i class="fa fa-video-camera"></i>
                        </div>
                        <div class="info-blog">
                            <div class="single-b-info category-name">
                                <i class="fa fa-folder-open-o"></i>
                                <a href="#">
                                    <span>Video</span>
                                </a>
                            </div>
                            <div class="single-b-info createdby">
                                <i class="fa fa-user"></i>
                                <span>Super User</span>
                            </div>
                            <div class="single-b-info hits">
                                <i class="fa fa-eye"></i>
                                <span>Hits: 256</span>
                            </div>
                            <div class="single-b-info post_rating">
                                Rating:
                                <div class="voting-symbol">
                                    <span class="star active"></span>
                                    <span class="star active"></span>
                                    <span class="star active"></span>
                                    <span class="star"></span>
                                    <span class="star"></span>
                                </div>
                            </div>
                            <h2 class="name">
                                <a href="#">Takiullah Tayeb Rayed</a>
                            </h2>
                        </div>
                    </div>
                    <p>Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus. Praesent ornare tortor ac ante egestas hendrerit.</p>
                    <div class="blog-comments-links">
                        <a class="readmore-link" href="#" title="Images">Read more ...</a>
                        <a class="comments-link" href="#" title="1 comment">1 comment</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="blog-right-sidebar">
                    <h3 class="sp-module-title">
                        <span>Newsletter</span>
                    </h3>
                    <div class="sub-area">
                        <form action="#">
                            <input type="text" placeholder="E-mail">
                            <input type="submit" value="Subscribe">
                        </form>
                    </div>
                    <div class="latest-posts">
                        <h3 class="sp-module-title">
                            <span>Latest Posts</span>
                        </h3>
                        <div class="single-l-post">
                            <a href="#">Images</a>
                            <p>23 May 2015</p>
                        </div>
                        <div class="single-l-post">
                            <a href="#">Video</a>
                            <p>23 May 2015</p>
                        </div>
                        <div class="single-l-post">
                            <a href="#">Takiullah Tayeb Rayed</a>
                            <p>2 Jun 2015</p>
                        </div>
                        <div class="single-l-post">
                            <a href="#">shuvo</a>
                            <p>20 Oct 2015</p>
                        </div>
                        <div class="single-l-post">
                            <a href="#">Tayeb Rayed</a>
                            <p>6 Sept 2015</p>
                        </div>
                        <div class="single-l-post">
                            <a href="#">Sarmin Akter Santa</a>
                            <p>7 Sept 2015</p>
                        </div>
                        <div class="single-l-post">
                            <a href="#">Tasnim</a>
                            <p>8 Sept 2015</p>
                        </div>
                    </div>
                    <div class="add-r-sidebar">
                        <p class="banner-block">
                            <a href="#">
                                <img alt="" src="img/blog/3_002.jpg">
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>