<!doctype html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7 ]> <html lang="en" class="ie6">    <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="">
    
<!-- Mirrored from devitems.com/html/castar-preview/castar/404-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Aug 2016 04:20:11 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>404 Not Found || <?php echo $this->Options->get('name'); ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Favicon
		============================================ -->
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico"> 

        <!-- Fonts  -->
        <link href='https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700,800' rel='stylesheet' type='text/css' />

 		<!-- CSS  -->
		
		<!-- Bootstrap CSS
		============================================ -->      
        <link rel='stylesheet' type='text/css' href="<?php echo assets('css/bootstrap.min.css'); ?>">
		
		<!-- font-awesome.min CSS
		============================================ -->      
        <link rel='stylesheet' type='text/css' href="<?php echo assets('css/font-awesome.min.css'); ?>">
		
		<!-- owl.carousel CSS
		============================================ -->      
        <link rel='stylesheet' type='text/css' href="<?php echo assets('css/owl.carousel.css'); ?>">
        
		<!-- owl.theme CSS
		============================================ -->      
        <link rel='stylesheet' type='text/css' href="<?php echo assets('css/owl.theme.css'); ?>">
           	
		<!-- owl.transitions CSS
		============================================ -->      
        <link rel='stylesheet' type='text/css' href="<?php echo assets('css/owl.transitions.css'); ?>">
		
		<!-- animate CSS
		============================================ -->         
        <link rel='stylesheet' type='text/css' href="<?php echo assets('css/animate.css'); ?>">
		
        <!-- meanmenu CSS
        ============================================ -->         
        <link rel='stylesheet' type='text/css' href="<?php echo assets('css/meanmenu.min.css'); ?>">

		<!-- cloud-zoom CSS
		============================================ --> 
        <link rel='stylesheet' type='text/css' href="<?php echo assets('css/cloud-zoom.css'); ?>">
		
		<!-- main CSS
		============================================ -->
        <link rel='stylesheet' type='text/css' href="<?php echo assets('css/main.css'); ?>">
		
		<!-- nivo-slider CSS
        ============================================ -->          
        <link rel='stylesheet' type='text/css' href="<?php echo assets('css/nivo-slider.css'); ?>">

		<!-- style CSS
		============================================ -->          
        <link rel='stylesheet' type='text/css' href="<?php echo assets('style.css'); ?>">
        
        <!-- responsive CSS
		============================================ -->          
        <link rel='stylesheet' type='text/css' href="<?php echo assets('css/responsive.css'); ?>">
		
        <script src="<?php echo assets('js/vendor/modernizr-2.8.3.min.js'); ?>"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

			<!-- Start page content -->
            <section class="not-found">
                <div class="container">
                	<div class="row">
                		<div class="col-sm-12">
                			<div class="page-not-found">
                				<a href="#"><img src="<?php echo assets('img/logo.png'); ?>" alt=""></a>
                				<h2>404</h2>
                				<p>Waah, Halaman yang anda Cari Tidak di temukan, klik tombol di bawah ini.</p>
                				<p><a href="<?php echo base_url(); ?>">go to home</a>
								
                                </p>
							</div>
                		</div>
                	</div>
                </div>
            </section>
            <!-- End contact page area -->
		
		
		<!-- jquery-1.12.0.min js
		============================================ -->  
        <script src="<?php echo assets('js/vendor/jquery-1.12.3.min.js'); ?>"></script>
		
		<!-- bootstrap js
		============================================ -->         
        <script src="<?php echo assets('js/bootstrap.min.js'); ?>"></script>
		
		<!-- owl.carousel.min js
		============================================ -->       
        <script src="<?php echo assets('js/owl.carousel.min.js'); ?>"></script>

        <!-- cloud-zoom js
		============================================ -->       
        <script src="<?php echo assets('js/cloud-zoom.js'); ?>"></script>
		
		<!-- jquery.nivo.slider.pack js
        ============================================ -->       
        <script src="<?php echo assets('js/jquery.nivo.slider.pack.js'); ?>"></script>

		<!-- wow js
		============================================ -->       
        <script src="<?php echo assets('js/wow.js'); ?>"></script>

        <!-- scrollUp js
        ============================================ -->       
        <script src="<?php echo assets('js/jquery.scrollUp.min.js'); ?>"></script>
	
        <!-- jquery.meanmenu js
        ============================================ -->       
        <script src="<?php echo assets('js/jquery.meanmenu.js'); ?>"></script>

        <!-- plugins js
		============================================ -->         
        <script src="<?php echo assets('js/plugins.js'); ?>"></script>
        
   		<!-- main js
		============================================ -->           
        <script src="<?php echo assets('js/main.js'); ?>"></script>
    </body>
</html>