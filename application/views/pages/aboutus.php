<section class="main_shop_area">
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="container-inner">
                        <ul>
                            <li class="home">
                                <a href="#">Home</a>
                                <span>
                                    <i class="fa fa-angle-right"></i>
                                </span>
                            </li>
                            <li class="category3">
                                <strong>About US</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="about-optima-text">
                        <h1>
                            About  
                            <strong><?php echo $this->Options->get('name'); ?></strong>
                        </h1>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.Donec pede justo, fringilla vel,</p>
<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes</p>
                        <ul>
                            <li>We love products that work perfectly and look beautiful.</li>
                            <li>We create base on a deeply analysis of your project.</li>
                            <li>We are create design with really high quality standards.</li>
                        </ul>
                        <a class="learn_btn" href="#">Learn More</a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="about-optima-img">
                        <img class="primary-image" src="<?php echo assets('img/about/team30.jpg'); ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="p10"></div>