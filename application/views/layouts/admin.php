<!DOCTYPE html>
<html>

<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- App favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo assets($this->Options->get('logo')); ?>">
        <!-- App title -->
        <title>
            <?php
                if (!isset($title)) {
                    echo $this->Options->get('name') . ' || ' . $this->Options->get('slogan');
                } else {
                    echo $title . ' || ' . $this->Options->get('name');
                }
            ?>
        </title>

        <!--Morris Chart CSS -->
		<link rel="stylesheet" href="<?php echo privassets('plugins/morris/morris.css'); ?>">

        <!-- App css -->
        <link href="<?php echo privassets('css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo privassets('css/core.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo privassets('css/components.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo privassets('css/icons.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo privassets('css/pages.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo privassets('css/menu.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo privassets('css/responsive.css'); ?>" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="<?php echo privassets('plugins/switchery/switchery.min.css'); ?>">

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo privassets('js/modernizr.min.js'); ?>"></script>

        <!-- Custom Headers
        ============================================ -->
        <?php if (isset($header)) {
                echo $header;
            }
        ?>

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="<?php echo base_url('_admin/home'); ?>" class="logo"><span><?php echo $this->Options->get('name'); ?></span><i class="mdi mdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">


                        <!-- Right(Notification) -->
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#" class="right-menu-item dropdown-toggle" data-toggle="dropdown">
                                    <i class="mdi mdi-bell"></i>
                                    <span class="badge up bg-success">4</span>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right dropdown-lg user-list notify-list">
                                    <li>
                                        <h5>Notifications</h5>
                                    </li>
                                    <li>
                                        <a href="#" class="user-list-item">
                                            <div class="icon bg-info">
                                                <i class="mdi mdi-account"></i>
                                            </div>
                                            <div class="user-desc">
                                                <span class="name">New Signup</span>
                                                <span class="time">5 hours ago</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="user-list-item">
                                            <div class="icon bg-danger">
                                                <i class="mdi mdi-comment"></i>
                                            </div>
                                            <div class="user-desc">
                                                <span class="name">New Message received</span>
                                                <span class="time">1 day ago</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="user-list-item">
                                            <div class="icon bg-warning">
                                                <i class="mdi mdi-settings"></i>
                                            </div>
                                            <div class="user-desc">
                                                <span class="name">Settings</span>
                                                <span class="time">1 day ago</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="all-msgs text-center">
                                        <p class="m-0"><a href="#">See all Notification</a></p>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript:void(0);" class="right-bar-toggle right-menu-item">
                                    <i class="mdi mdi-settings"></i>
                                </a>
                            </li>

                            <li class="dropdown user-box">
                                <a href="#" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
                                    <img src="<?php echo privassets('images/male-avatar.png'); ?>" alt="user-img" class="img-circle user-img">
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                                    <li>
                                        <h5><?php echo $user->full_name; ?></h5>
                                    </li>
                                    <li><a href="<?php echo base_url('_admin/accounts/profile'); ?>"><i class="ti-user m-r-5"></i> Settings</a></li>
                                    <li><a href="<?php echo base_url('_admin/logout'); ?>"><i class="ti-power-off m-r-5"></i> Logout</a></li>
                                </ul>
                            </li>

                        </ul> <!-- end navbar-right -->

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <?php $this->load->view('admin/partial/_sidebar'); ?>
                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <div class="content">
                    <?php
                        if (isset($content)) {
                            echo $content;
                        }
                    ?>
                </div> <!-- content -->

                <footer class="footer text-right">
                    <?php echo date('Y'); ?> © <?php echo $this->Options->get('name'); ?>.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar">
                <a href="javascript:void(0);" class="right-bar-toggle">
                    <i class="mdi mdi-close-circle-outline"></i>
                </a>
                <h4 class="">Settings</h4>
                <div class="setting-list nicescroll">
                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Notifications</h5>
                            <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">API Access</h5>
                            <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Auto Updates</h5>
                            <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Online Status</h5>
                            <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo privassets('js/jquery.min.js'); ?>"></script>
        <script src="<?php echo privassets('js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo privassets('js/detect.js'); ?>"></script>
        <script src="<?php echo privassets('js/fastclick.js'); ?>"></script>
        <script src="<?php echo privassets('js/jquery.blockUI.js'); ?>"></script>
        <script src="<?php echo privassets('js/waves.js'); ?>"></script>
        <script src="<?php echo privassets('js/jquery.slimscroll.js'); ?>"></script>
        <script src="<?php echo privassets('js/jquery.scrollTo.min.js'); ?>"></script>
        <script src="<?php echo privassets('plugins/switchery/switchery.min.js'); ?>"></script>

        <!-- Counter js  -->
        <script src="<?php echo privassets('plugins/waypoints/jquery.waypoints.min.js'); ?>"></script>
        <script src="<?php echo privassets('plugins/counterup/jquery.counterup.min.js'); ?>"></script>

        <!-- Flot chart js -->
        <script src="<?php echo privassets('plugins/flot-chart/jquery.flot.min.js'); ?>"></script>
        <script src="<?php echo privassets('plugins/flot-chart/jquery.flot.time.js'); ?>"></script>
        <script src="<?php echo privassets('plugins/flot-chart/jquery.flot.tooltip.min.js'); ?>"></script>
        <script src="<?php echo privassets('plugins/flot-chart/jquery.flot.resize.js'); ?>"></script>
        <script src="<?php echo privassets('plugins/flot-chart/jquery.flot.pie.js'); ?>"></script>
        <script src="<?php echo privassets('plugins/flot-chart/jquery.flot.selection.js'); ?>"></script>
        <script src="<?php echo privassets('plugins/flot-chart/jquery.flot.crosshair.js'); ?>"></script>

        <script src="<?php echo privassets('plugins/moment/moment.js'); ?>"></script>
        <script src="<?php echo privassets('plugins/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>


        <!-- Dashboard init -->
        <script src="<?php echo privassets('pages/jquery.dashboard_2.js'); ?>"></script>

        <!-- App js -->
        <script src="<?php echo privassets('js/jquery.core.js'); ?>"></script>
        <script src="<?php echo privassets('js/jquery.app.js'); ?>"></script>

        <script>
            $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
            $('#reportrange').daterangepicker({
                format: 'MM/DD/YYYY',
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: '12/31/2016',
                dateLimit: {
                    days: 60
                },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'left',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-success',
                cancelClass: 'btn-default',
                separator: ' to ',
                locale: {
                    applyLabel: 'Submit',
                    cancelLabel: 'Cancel',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            });
        </script>
        <?php
            if (isset($footer)) {
                echo $footer;
            }
        ?>

    </body>

</html>
