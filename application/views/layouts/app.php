<!doctype html>
<html class="no-js" lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>
			
			<?php 
                 if (!isset($title)) {
                     echo $this->Options->get('name') . ' || ' . $this->Options->get('slogan');
                 } else {
                     echo $title . ' || ' . $this->Options->get('name');
                 }
            ?>

		</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- favicon
		============================================ -->
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
		<!-- Google Fonts
		============================================ -->
		<link href='//fonts.googleapis.com/css4c5c.css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
		<link href='//fonts.googleapis.com/css4b01.css?family=Lato:400,300,700,900' rel='stylesheet' type='text/css'>
		<!-- Bootstrap CSS
		============================================ -->
		<link rel="stylesheet" href="<?php echo assets('css/bootstrap.min.css');?>">
		<!-- Bootstrap CSS
		============================================ -->
		<link rel="stylesheet" href="<?php echo assets('css/font-awesome.min.css');?>">
		<!-- owl.carousel CSS
		============================================ -->
		<link rel="stylesheet" href="<?php echo assets('css/owl.carousel.css');?>">
		<link rel="stylesheet" href="<?php echo assets('css/owl.theme.css');?>">
		<link rel="stylesheet" href="<?php echo assets('css/owl.transitions.css');?>">
		<!-- nivo slider CSS
		============================================ -->
		<link rel="stylesheet" href="<?php echo assets('lib/css/nivo-slider.css');?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo assets('lib/css/preview.css');?>" type="text/css" media="screen" />
		<!-- animate CSS
		============================================ -->
		<link rel="stylesheet" href="<?php echo assets('css/animate.css');?>">
		<!-- meanmenu CSS
		============================================ -->
		<link rel="stylesheet" href="<?php echo assets('css/meanmenu.min.css');?>">
		<!-- Image Zoom CSS
		============================================ -->
		<link rel="stylesheet" href="<?php echo assets('css/img-zoom/jquery.simpleLens.css');?>">
		<!-- normalize CSS
		============================================ -->
		<link rel="stylesheet" href="<?php echo assets('css/normalize.css');?>">
		<!-- main CSS
		============================================ -->
		<link rel="stylesheet" href="<?php echo assets('css/main.css');?>">
		<!-- style CSS
		============================================ -->
		<link rel="stylesheet" href="<?php echo assets('style.css');?>">
		<!-- responsive CSS
		============================================ -->
		<link rel="stylesheet" href="<?php echo assets('css/responsive.css');?>">
		<!-- modernizr JS
		============================================ -->
		<script src="<?php echo assets('js/vendor/modernizr-2.8.3.min.js');?>"></script>
		<?php
            if (isset($header)) {
                echo $header;
            }
        ?>
		
	</head>
	<body class="home-1">
		<!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->
		<!-- Add your site or application content here -->
		<!-- start header_area
		============================================ -->
			<?php echo $this->load->view('partial/_header', false, true); ?>	
		<!-- end header_area
		============================================ -->
		<?php
            if (isset($content)) {
                echo $content;
            }
        ?>
		<!-- start ma-footer-stati
		============================================ -->
		<div class="ma-footer-static ma-footer-top">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="container-inner">
							<nav>
								<ul class="pt-menu-links">
									<li class="first home">
										<a href="#">Home</a>
									</li>
									<li>
										<a href="#">about us</a>
									</li>
									<li>
										<a href="#">Contacts</a>
									</li>
									<li>
										<a href="#">Blog</a>
									</li>
									<li class="last">
										<a href="#">View All Themes</a>
									</li>
								</ul>
							</nav>
							<div class="footer-static-top">
								<div class="row">
									<div class="f-col f-col1 col-md-3 col-sm-4 col-xs-12">
										<div class="static_all">
											<div class="footer-static-title">
												<h3>My Account</h3>
											</div>
											<div class="footer-static-content">
												<ul>
													<li>
														<a href="#">My Account</a>
													</li>
													<li>
														<a href="#">Login</a>
													</li>
													<li>
														<a href="#">My Cart</a>
													</li>
													<li>
														<a href="#">Wishlist</a>
													</li>
													<li class="last">
														<a href="#">Checkout</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="f-col f-col2 col-md-3 hidden-sm col-xs-12">
										<div class="static_all">
											<div class="footer-static-title">
												<h3>Information</h3>
											</div>
											<div class="footer-static-content">
												<ul>
													<li>
														<a href="#">Sitemap</a>
													</li>
													<li>
														<a href="#">Privacy Policy</a>
													</li>
													<li>
														<a href="#">Advanced Search</a>
													</li>
													<li>
														<a href="#">Privacy Policy</a>
													</li>
													<li class="last">
														<a href="#">Contact Us</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="f-col f-col3 col-md-3 col-sm-4 col-xs-12">
										<div class="static_all">
											<div class="footer-static-title">
												<h3>Customer Service</h3>
											</div>
											<div class="footer-static-content">
												<ul>
													<li>
														<a href="#">Product Recall</a>
													</li>
													<li>
														<a href="#">Gift Vouchers</a>
													</li>
													<li>
														<a href="#">Returns and Exchanges</a>
													</li>
													<li>
														<a href="#">Shipping Options</a>
													</li>
													<li class="last">
														<a href="#">Gift Vouchers</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="f-col f-col4 col-md-3 col-sm-4 col-xs-12">
										<div class="footer-static-title">
											<h3>Store information</h3>
										</div>
										<div class="footer-static-content">
											<div class="footer-contact">
												<p class="adress">My Company</p>
												<p class="phone">42 avenue des Champs Elysées 75000 Paris France</p>
												<p class="fax">Call us now: (+1)866-540-3229</p>
												<p class="email">Email: admin@Bootexperts.com</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
            echo $this->load->view('partial/_footer', false, true);
           
            if (isset($footer)) {
                echo $footer;
            }
        ?>
	</body>
</html>