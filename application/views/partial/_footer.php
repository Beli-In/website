<!-- end ma-footer-stati
	============================================ -->
	<!-- end footer-address
	============================================ -->
	<footer class="footer-address">
		<div class="container">
			<div class="container-inner">
				<div class="row">
					<div class="col-md-6">
						<address>
							Copyright ©
							<a href="#">BootExperts.</a>
							All Rights Reserved
						</address>
					</div>
					<div class="col-md-6">
						<div class="footer-payment">
							<a href="#">
								<img alt="" src="img/footer/payment.png">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- end footer-address
	============================================ -->
	<!-- start scrollUp
	============================================ -->
	<div id="toTop">
		<i class="fa fa-chevron-up"></i>
	</div>
	<!-- end scrollUp
	============================================ -->
	<!-- jquery
	============================================ -->
	<script src="<?php echo assets('js/vendor/jquery-1.11.3.min.js');?>"></script>
	<!-- bootstrap JS
	============================================ -->
	<script src="<?php echo assets('js/bootstrap.min.js');?>"></script>
	<!-- wow JS
	============================================ -->
	<script src="<?php echo assets('js/wow.min.js');?>"></script>
	<!-- price-slider JS
	============================================ -->
	<script src="<?php echo assets('js/jquery-price-slider.js');?>"></script>
	<!-- Img Zoom js -->
	<script src="<?php echo assets('js/img-zoom/jquery.simpleLens.min.js');?>"></script>
	<!-- meanmenu JS
	============================================ -->
	<script src="<?php echo assets('js/jquery.meanmenu.js');?>"></script>
	<!-- owl.carousel JS
	============================================ -->
	<script src="<?php echo assets('js/owl.carousel.min.js');?>"></script>
	<!-- scrollUp JS
	============================================ -->
	<script src="<?php echo assets('js/jquery.scrollUp.min.js');?>"></script>
	<!-- Nivo slider js
	============================================ -->
	<script src="<?php echo assets('lib/js/jquery.nivo.slider.js');?>" type="text/javascript"></script>
	<script src="<?php echo assets('lib/home.js');?>" type="text/javascript"></script>
	<!-- plugins JS
	============================================ -->
	<script src="<?php echo assets('js/plugins.js');?>"></script>
	<!-- main JS
	============================================ -->
	<script src="<?php echo assets('js/main.js');?>"></script>