<?php

function fn($data) {
  return $data;
}
$fn = 'fn';

$html['content'] = <<<MARK
<div class="wishlists-page-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 np">
                            	<div id="checkoutMethod" class="panel-collapse collapse in">
                                    <div class="content-info">
                                        <div class="col-md-6">
                                            <div class="checkout-reg">
                                                <div class="checkReg commonChack">
                                                    <div class="checkTitle">
                                                        <h6 class="ct-design">New Customers</h6>
                                                    </div>
                                                </div>
                                                <div class="regSaveTime commonChack">
                                                    <h6>REGISTER AND SAVE TIME!</h6>
                                                    <p>De</p>
                                                </div>
                                                <a href="{$fn(base_url('account/register'))}" class="create-account checkPageBtn pull-right">Create an Account</a>
                                            </div>
                                        </div>
                                        {{ $fn }}
                                        <div class="col-md-6">
                                            <div class="checkout-login">
                                                    <div class="checkTitle">
                                                        <h6 class="ct-design">Login</h6>
                                                    </div>
                                                <p class="alrdyReg">ALREADY REGISTERED?</p>
                                                <p class="plxLogin">Please log in below:</p>
                                                <div class="loginFrom">
                                                    <p class="plxLoginP"><span>*</span> Email Address</p>
                                                    <input type="text"><br>
                                                    <p class="plxLoginP"><span>*</span>  Password</p>
                                                    <input type="text">
                                                    <p class="rqudField">* Required Fields</p>
                                                    <p class="plxLogin">Forgot your password ?</p>
                                                </div>
                                                <a href="#" class="checkPageBtn pull-right">Login</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
MARK;
echo $this->load->view('pages/blank', $html, TRUE);