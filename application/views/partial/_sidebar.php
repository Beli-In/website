<header class="header_area">
    <div class="top-link">
        <div class="container">
            <div class="row">
                <div class="col-md-11 col-sm-11">
                    <div class="top-logo">
                        <a href="index.html"><img src="<?php echo base_url($this->Options->get('logo')); ?> " alt=""></a>
                    </div>
                    <div class="pull-right">
                        <div class="call_us">
                            <p>
                                Hubungi Kami
                                <span> <?php echo $this->Options->get('telp'); ?></span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 col-sm-1">
                    <div class="english">
                        <ul>
                            <li class="dropit-trigger">
                                <a class="parent-link login_click" href="#"><i class="fa fa-key"></i></a>
                                <ul class="submenu">
                                    <li><a href="#">My Account</a></li>
                                    <li><a href="#">My Wishlist</a></li>
                                    <li><a href="#">My Cart</a></li>
                                    <li><a href="#">Checkout</a></li>
                                    <li><a href="#">Log In</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <form action="#" id="search_mini_form">
                        <div class="form-search s-same" >
                            <div class="select-wrapper">
                                <select class="select">
                                    <option value="">All Categories</option>
                                    <option value="">Smartphones & Accessories</option>
                                    <option value="">Computers & Networking</option>
                                    <option value="">Laptops & Tablets</option>
                                    <option value="">Camerea & Camcorders</option>
                                    <option value="">Watches</option>
                                    <option value="">Lights & Lighting</option>
                                    <option value="">Air conditioner</option>
                                </select>
                            </div>
                            <input class="input-text" type="text" placeholder="Search entire store here">
                            <button class="button" title="Search" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="header-wrapper">
                        <div class="top-cart-wrapper">
                            <div class="top-cart-contain">
                                <div class="block-cart">
                                    <div class="top-cart-title">
                                        <div class="my-cart">Shopping cart</div>
                                        <a href="#"><p>(2) item:<span>$100.00</span></p></a>
                                    </div>
                                    <div class="home">
                                        <ul>
                                            <li>
                                                <div class="cat">
                                                    <a href="#">
                                                        <img src="img/product/4.jpg" alt="">
                                                    </a>
                                                    <div class="cat_two">
                                                        <p>
                                                            <a href="#">Quisque in arcu</a>
                                                        </p>
                                                        <p>1 x $50.00</p>
                                                    </div>
                                                    <div class="cat_icon">
                                                        <a href="#">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="cat">
                                                    <a href="#">
                                                        <img src="img/product/5_1_1.jpg" alt="">
                                                    </a>
                                                    <div class="cat_two">
                                                        <p>
                                                            <a href="#">Donec non est</a>
                                                        </p>
                                                        <p>1 x $50.00</p>
                                                    </div>
                                                    <div class="cat_icon">
                                                        <a href="#">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="cat_bottom">
                                                    <div class="cat_s">
                                                        <p>Subtotal:<span>$100.00</span></p>
                                                    </div>
                                                    <div class="cat_d">
                                                        <a href="#"><strong>Checkout</strong></a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="top-compare ">
                            <div class=" block-compare">
                                <div class="compare">
                                    <a href="#"><i class="fa fa-refresh"> </i>
                                        <small>(1)</small>
                                    </a>
                                </div>
                                <div class="home">
                                    <ul>
                                        <li>
                                            <div class="cat">
                                                <a href="#">
                                                    <img src="img/product/5_1_1.jpg" alt="">
                                                </a>
                                                <div class="cat_two">
                                                    <p>
                                                        <a href="#">Donec non est</a>
                                                    </p>
                                                    <p>1 x $721.00</p>
                                                </div>
                                                <div class="cat_icon">
                                                    <a href="#">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="cat_bottom">
                                                <div class="cat_s">
                                                    <p>Clear Al</p>
                                                </div>
                                                <div class="cat_up">
                                                    <a href="#"><strong>Compare</strong></a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="top-wishlist ">
                            <div class=" block-compare">
                                <div class="compare">
                                    <a href="#"><i class="fa fa-heart-o "> </i>
                                        <small>(1)</small>
                                    </a>
                                </div>
                                <div class="home" id="right">
                                    <ul>
                                        <li>
                                            <div class="cat">
                                                <a href="#">
                                                    <img src="img/product/4.jpg" alt="">
                                                </a>
                                                <div class="cat_two">
                                                    <p>
                                                        <a href="#">Quisque in arcu</a>
                                                    </p>
                                                    <p>1 x $721.00</p>
                                                </div>
                                                <div class="cat_icon">
                                                    <a href="#">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="cat_bottom">
                                                <div class="cat_s">
                                                    <p>Clear All</p>
                                                </div>
                                                <div class="cat_up">
                                                    <a href="#"><strong>Compare</strong></a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="top-menu">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-5">
                    <div class="left-category-menu  hidden-xs">
                        <div class="left-product-cat">
                            <div class="category-heading">
                                <h2>Category</h2>
                            </div>
                            <div class="category-menu-list">
                                <ul>
                                    <li class="arrow-plus">
                                        <a href="shop.html">
                                            <span class="cat-thumb">
                                                <img alt="" src="img/icon/1.png">
                                            </span>
                                            Smartphones & Accessories
                                        </a>
                                        <div class="cat-left-drop-menu ">
                                            <div class="cat-left-drop-menu-left">
                                                <a class="menu-item-heading" href="#">Dresses</a>
                                                <ul>
                                                    <li>
                                                        <a href="#">Cocktail</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Day</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Evening</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Sports</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="cat-left-drop-menu-left">
                                                <a class="menu-item-heading" href="#">shoes</a>
                                                <ul>
                                                    <li>
                                                        <a href="#">Sports</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">run</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">sandals</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Books</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="cat-left-drop-menu-left">
                                                <a class="menu-item-heading" href="#">Handbags</a>
                                                <ul>
                                                    <li>
                                                        <a href="#">Blazers</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">table</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">coats</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">kids</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="cat-left-drop-menu-left">
                                                <a class="menu-item-heading" href="#">Clothing</a>
                                                <ul>
                                                    <li>
                                                        <a href="#">T-shirts</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">coats</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Jackets </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">jeans</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="arrow-plus">
                                        <a href="shop.html">
                                            <span class="cat-thumb">
                                                <img alt="" src="img/icon/2.png">
                                            </span>
                                            Computers & Networking
                                        </a>
                                        <div class="cat-left-drop-menu">
                                            <div class="cat-left-drop-menu-left">
                                                <a class="menu-item-heading" href="#">Bags </a>
                                                <ul>
                                                    <li>
                                                        <a href="#">Bootees  Bags</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Blazers</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="cat-left-drop-menu-left">
                                                <a class="menu-item-heading" href="#">Clothing </a>
                                                <ul>
                                                    <li>
                                                        <a href="#">coats</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">T-shirts</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="cat-left-drop-menu-left">
                                                <a class="menu-item-heading" href="#">Lingerie </a>
                                                <ul>
                                                    <li>
                                                        <a href="#">Bands</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Furniture</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="cat-left-drop-menu-left">
                                                <a class="menu-item-heading" href="#">Handbag</a>
                                                <ul>
                                                    <li>
                                                        <a href="#">Bands</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Funiture</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Lifestyle</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="arrow-plus">
                                        <a href="shop.html">
                                            <span class="cat-thumb">
                                                <img alt="" src="img/icon/3.png">
                                            </span>
                                            Laptops & Tablets
                                        </a>
                                        <div class="cat-left-drop-menu">
                                            <div class="cat-left-drop-menu-left">
                                                <a class="menu-item-heading" href="#">Footwear Man</a>
                                                <ul>
                                                    <li>
                                                        <a href="#">Gold Ring</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Platinum Rings</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Silver Ring</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Tungsten Ring</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="cat-left-drop-menu-left">
                                                <a class="menu-item-heading" href="#">Footwear Womens</a>
                                                <ul>
                                                    <li>
                                                        <a href="#">Bands Gold</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Platinum Bands</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Silver Bands</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Tungsten Bands</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="cat-left-drop-menu-left">
                                                <a class="menu-item-heading" href="#">Rings</a>
                                                <ul>
                                                    <li>
                                                        <a href="#">Platinum Bracelets</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Gold Ring</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Silver Ring</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Diamond Bracelets</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="cat-left-drop-menu-left">
                                                <a class="menu-item-heading" href="#">Bands</a>
                                                <ul>
                                                    <li>
                                                        <a href="#">Platinum Necklaces </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Gold Ring</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Silver Ring</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Sunglasses</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="arrow-plus">
                                        <a href="shop.html">
                                            <span class="cat-thumb">
                                                <img alt="" src="img/icon/4.png">
                                            </span>
                                            Camerea & Camcorders
                                        </a>
                                        <div class="cat-left-drop-menu">
                                            <div class="cat-left-drop-menu-left">
                                                <a class="menu-item-heading" href="#">Rings</a>
                                                <ul>
                                                    <li>
                                                        <a href="#">Coats & Jackets</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Blazers</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Jackets</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Raincoats</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="cat-left-drop-menu-left">
                                                <a class="menu-item-heading" href="#">Dresses</a>
                                                <ul>
                                                    <li>
                                                        <a href="#">Ankle Boots</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Footwear </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Clog sandals</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Combat Boots</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="cat-left-drop-menu-left">
                                                <a class="menu-item-heading" href="#">Accessories</a>
                                                <ul>
                                                    <li>
                                                        <a href="#">Bootees Bags</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Blazers</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Jackets</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Shoes</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="cat-left-drop-menu-left">
                                                <a class="menu-item-heading" href="#">Top</a>
                                                <ul>
                                                    <li>
                                                        <a href="#">Briefs</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Camis</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Nightwear</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Shapewear</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="shop.html">
                                            <span class="cat-thumb ">
                                                <img alt="" src="img/icon/5.png">
                                            </span>
                                            Watches
                                        </a>
                                    </li>
                                    <li>
                                        <a href="shop.html">
                                            <span class="cat-thumb">
                                                <img alt="" src="img/icon/6.png">
                                            </span>
                                            Lights & Lighting
                                        </a>
                                    </li>
                                    <li class=" rx-child" style="display: none;">
                                        <a href="shop.html">
                                            <span class="cat-thumb"></span>
                                            clothing
                                        </a>
                                    </li>
                                    <li class="rx-parent">
                                        <a class="rx-default">
                                            <span class="cat-thumb fa fa-plus"></span>
                                            More categories
                                        </a>
                                        <a class="rx-show">
                                            <span class="cat-thumb fa fa-minus"></span>
                                            close menu
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-7">
                    <div class="home_menu">
                        <nav>
                            <ul>
                                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                                <li><a href="<?php echo base_url('products'); ?>">Browser Products</a></li>
                                <li><a href="<?php echo base_url('about-us'); ?>">about us</a></li>
                                <li><a href="<?php echo base_url('news'); ?>">News</a></li>
                                <li><a href="<?php echo base_url('contact-us'); ?>">Contacts</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- mobile-menu-area start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul>
                                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                                <li><a href="<?php echo base_url('products'); ?>">Browser Products</a></li>
                                <li><a href="<?php echo base_url('about-us'); ?>">about us</a></li>
                                <li><a href="<?php echo base_url('news'); ?>">News</a></li>
                                <li><a href="<?php echo base_url('contact-us'); ?>">Contacts</a></li>
                            </ul>
                        </nav>
                    </div>                  
                </div>
            </div>
        </div>
    </div>
    <!-- mobile-menu-area end -->               
</header>