<!doctype html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7 ]> <html lang="en" class="ie6">    <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="">
    
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Beli-In || You Happy We Happy</title>
        <meta name="description" content="">
		<meta name="keyword" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Favicon
		============================================ -->
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico"> 

        <!-- Fonts  -->
        <link href='https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700,800' rel='stylesheet' type='text/css' />

 		<!-- CSS  -->
		
		<!-- Bootstrap CSS
		============================================ -->      
        <link src="<?php echo assets('css/bootstrap.min.css'); ?>">
		
		<!-- font-awesome.min CSS
		============================================ -->      
        <link src="<?php echo assets('css/font-awesome.min.css'); ?>">
		
		<!-- owl.carousel CSS
		============================================ -->      
        <link src="<?php echo assets('css/owl.carousel.css'); ?>">
        
		<!-- owl.theme CSS
		============================================ -->      
        <link src="<?php echo assets('css/owl.theme.css'); ?>">
           	
		<!-- owl.transitions CSS
		============================================ -->      
        <link src="<?php echo assets('css/owl.transitions.css'); ?>">
		
		<!-- animate CSS
		============================================ -->         
        <link src="<?php echo assets('css/animate.css'); ?>">
		
		<!-- meanmenu CSS
        ============================================ -->         
        <link src="<?php echo assets('css/meanmenu.min.css'); ?>">

		<!-- normalize CSS
		============================================ --> 
        <link src="<?php echo assets('css/normalize.css'); ?>">
		
		<!-- main CSS
		============================================ -->
        <link src="<?php echo assets('css/main.css'); ?>">
		
		<!-- nivo-slider CSS
        ============================================ -->          
        <link src="<?php echo assets('css/nivo-slider.css'); ?>">

		<!-- style CSS
		============================================ -->          
        <link src="<?php echo assets('style.css'); ?>">
        
        <!-- responsive CSS
		============================================ -->          
        <link src="<?php echo assets('css/responsive.css'); ?>">
		
        <script src="<?php echo assets('js/vendor/modernizr-2.8.3.min.js'); ?>"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

		<!-- Start main area -->
		<div class="main-area home-three">
			<!-- Start header -->
			<header>
				<!-- Start logo and cart -->
				<div class="logo-and-cart">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-5">
								<div class="logo">
									<a href="index.html"><img src="img/logo.png" alt=""></a>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-7">
								<div class="header-link">
									<ul class="links">
										<li class="first" style="visibility: hidden;"><a title="My Account" href="my-account.html">My Account</a></li>
										<li style="visibility: hidden;"><a title="My Wishlist" href="wishlists.html">My Wishlist</a></li>
										<li style="visibility: hidden;"><a class="top-link-cart" title="My Cart" href="cart.html">My Cart</a></li>
										<li><a class="top-link-checkout" title="Checkout" href="checkout.html">Register</a></li>
										<li class=" last"><a class="a-login-link" title="Log In" href="login.html">Log In</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- End logo and cart -->
				<!-- Start main menu and search -->
				<div class="main-menu-and-search">
					<div class="container">
						<div class="menu-area">
							<div class="row">
								<div class="main-menu">
									<div class="col-sm-8">
										<nav>
											<ul>
												<li><a href="index.html"><i class="fa fa-home" aria-hidden="true"></i></a>
													<div class="mega-menu blog">
														<div class="menu-inner">
															<ul>
																<li><a href="index-2.html">Home2</a></li>
																<li><a href="index-3.html">Home3</a></li>
																<li><a href="index-4.html">Home4</a></li>
															</ul>
														</div>
													</div>
												</li>
												<li><a href="about-us.html">About us</a></li>
												<li><a href="#">women</a>
													<div class="mega-menu">
														<div class="menu-inner">
															<span><a href="shop-grid.html">Dresses</a></span>
															<ul>
																<li class="first"><a href="shop-grid.html">Cocktail</a></li>
																<li><a href="shop-grid.html">Day</a></li>
																<li><a href="shop-grid.html">Evening</a></li>
																<li><a href="shop-grid.html">Sports</a></li>
															</ul>
														</div>
														<div class="menu-inner">
															<span><a href="shop-grid.html">shoes</a></span>
															<ul>
																<li class="first"><a href="shop-grid.html">Sports</a></li>
																<li><a href="shop-grid.html">run</a></li>
																<li><a href="shop-grid.html">sandals</a></li>
																<li><a href="shop-grid.html">Books</a></li>
															</ul>
														</div>
														<div class="menu-inner">
															<span><a href="shop-grid.html">Handbags</a></span>
															<ul>
																<li class="first"><a href="shop-grid.html">Blazers</a></li>
																<li><a href="shop-grid.html">table</a></li>
																<li><a href="shop-grid.html">coats</a></li>
																<li><a href="shop-grid.html">kids</a></li>
															</ul>
														</div>
														<div class="menu-inner">
															<span><a href="shop-grid.html">Clothing</a></span>
															<ul>
																<li class="first"><a href="shop-grid.html">T-shirts</a></li>
																<li><a href="shop-grid.html">coats</a></li>
																<li><a href="shop-grid.html">Jackets</a></li>
																<li><a href="shop-grid.html">jeans</a></li>
															</ul>
														</div>
													</div>
												</li>
												<li><a href="#">men</a>
													<div class="mega-menu men">
														<div class="menu-inner">
															<span><a href="shop-grid.html">Dresses</a></span>
															<ul>
																<li class="first"><a href="shop-grid.html">Cocktail</a></li>
																<li><a href="shop-grid.html">Day</a></li>
																<li><a href="shop-grid.html">Evening</a></li>
																<li><a href="shop-grid.html">Sports</a></li>
															</ul>
														</div>
														<div class="menu-inner">
															<span><a href="#">shoes</a></span>
															<ul>
																<li class="first"><a href="shop-grid.html">Sports</a></li>
																<li><a href="shop-grid.html">run</a></li>
																<li><a href="shop-grid.html">sandals</a></li>
																<li><a href="shop-grid.html">Books</a></li>
															</ul>
														</div>
														<div class="menu-inner">
															<span><a href="shop-grid.html">Handbags</a></span>
															<ul>
																<li class="first"><a href="shop-grid.html">Blazers</a></li>
																<li><a href="shop-grid.html">table</a></li>
																<li><a href="shop-grid.html">coats</a></li>
																<li><a href="shop-grid.html">kids</a></li>
															</ul>
														</div>
														<div class="menu-inner">
															<span><a href="shop-grid.html">Clothing</a></span>
															<ul>
																<li class="first"><a href="#">T-shirts</a></li>
																<li><a href="shop-grid.html">coats</a></li>
																<li><a href="shop-grid.html">Jackets</a></li>
																<li><a href="shop-grid.html">jeans</a></li>
															</ul>
														</div>
													</div>
												</li>
												<li><a href="#">Blog</a>
													<div class="mega-menu blog">
														<div class="menu-inner">
															<ul>
																<li><a href="blog.html">Blog</a></li>
																<li><a href="blog-non-sidebar.html">Blog non sidebar</a></li>
																<li><a href="single-blog.html">Single blog</a></li>
															</ul>
														</div>
													</div>
												</li>
												<li><a href="#">Pages</a>
													<div class="mega-menu blog">
														<div class="menu-inner">
															<ul>
																<li><a href="about-us.html">About us</a></li>
																<li><a href="blog.html">Blog</a></li>
																<li><a href="blog-non-sidebar.html">Blog non sidebar</a></li>
																<li><a href="single-blog.html">Single blog</a></li>
																<li><a href="shop-grid.html">shop grid</a></li>
																<li><a href="shop-list.html">shop list</a></li>
																<li><a href="single-product.html">single product</a></li>
																<li><a href="cart.html">cart</a></li>
																<li><a href="wishlists.html">wishlists</a></li>
																<li><a href="checkout.html">checkout</a></li>
																<li><a href="contact-us.html">contact us</a></li>
																<li><a href="login.html">Login</a></li>
																<li><a href="register.html">Register</a></li>
																<li><a href="my-account.html">My account</a></li>
																<li><a href="404.html">404</a></li>
																<li><a href="404-2.html">404-2</a></li>
															</ul>
														</div>
													</div>
												</li>
												<li><a href="contact-us.html">Contact</a></li>
											</ul>
										</nav>
									</div>
								</div>
								<div class="search-area">
									<div class="col-sm-4">
										<nav>
											<ul>
												<li>
													<div class="search-icon">
														<i class="fa fa-search" aria-hidden="true"></i>
														<div class="search-box">
															<div class="search-content">
												                <input type="text" maxlength="128" class="input-text" value="" name="q" id="search" placeholder="search..." autocomplete="off">
												                <button class="button" title="Search" type="submit"><span><i class="fa fa-search"></i></span></button>
												            </div>
														</div>
													</div>
												</li>
												<li>
													<div class="cart-icon">
														<img src="img/icon/bkg_top_cart.png" alt="">
														<div class="cart-box">
															<div class="cart-content top-cart-content">
												                <!--<ul id="cart-sidebar" class="mini-products-list">
												                	<li class="item odd">
															            <a class="product-image" title="Aliquam consequat" href="#">
															            	<img width="50" height="66" alt="Aliquam consequat" src="img/product-mini/11_1.jpg">
															            </a>
																        <div class="product-details">
																	        <a class="btn-remove" title="Remove This Item" href="#">Remove This Item</a>
																	        <a class="btn-edit" title="Edit item" href="#">Edit item</a>
																	        <p class="product-name"><a href="#">Aliquam </a></p>
																	        <strong>1</strong> x
																			<span class="price">$123.00</span>                    
																		</div>
																	</li>
																	<li class="item odd">
															            <a class="product-image" title="Aliquam consequat" href="#">
															            	<img width="50" height="66" alt="Aliquam consequat" src="img/product-mini/08.jpg">
															            </a>
																        <div class="product-details">
																	        <a class="btn-remove" title="Remove This Item" href="#">Remove This Item</a>
																	        <a class="btn-edit" title="Edit item" href="#">Edit item</a>
																	        <p class="product-name"><a href="#">Proin lectus</a></p>
																	        <strong>1</strong> x
																			<span class="price">$88.00</span>                   
																		</div>
																	</li>
												                </ul>-->
																<div class="text-center">Empty Cart</div>
												                <div class="top-subtotal">Subtotal: <span class="price">$211.00</span></div>
												                <div class="actions">
												                	<a href="checkout.html">Checkout</a>
                        										</div>
												            </div>
														</div>
													</div>
												</li>
											</ul>
										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- End main menu and search -->
				<!-- Start mobile menu -->
                <div class="mobile-menu-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 np">
                                <div class="mobile-menu">
                                    <nav id="dropdown">
                                        <ul class="nav">
                                            <li><a class="home" href="index.html">Home</a>
                                                <ul>
                                                    <li><a href="index-2.html"><span>Home2</span></a></li>
                                                    <li><a href="index-3.html"><span>Home3</span></a></li>
                                                    <li><a href="index-4.html"><span>Home4</span></a></li>
                                                </ul>
                                            </li>
                                            <li><a href="about-us.html">About us</a></li>
                                            <li><a href="shop-grid.html">shop</a></li>
                                            <li><a href="#">Delivery</a></li>
                                            <li><a href="blog.html">Blog</a>
                                                <ul>
                                                    <li><a href="blog.html">Blog</a></li>
                                                    <li><a href="blog-non-sidebar.html">Blog non sidebar</a></li>
                                                    <li><a href="single-blog.html">Single blog</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Pages</a>
                                                <ul>
                                                    <li><a href="about-us.html"><span>About us</span></a></li>
                                                    <li><a href="blog.html"><span>Blog</span></a></li>
                                                    <li><a href="blog-non-sidebar.html"><span>blog non sidebar</span></a></li>
                                                    <li><a href="single-blog.html"><span>single blog</span></a></li>
                                                    <li><a href="shop-grid.html"><span>shop grid</span></a></li>
                                                    <li><a href="shop-list.html"><span>shop list</span></a></li>
                                                    <li><a href="single-product.html"><span>single product</span></a></li>
                                                    <li><a href="cart.html"><span>cart</span></a></li>
                                                    <li><a href="wishlists.html"><span>wishlists</span></a></li>
                                                    <li><a href="checkout.html"><span>checkout</span></a></li>
                                                    <li><a href="contact-us.html"><span>contact us</span></a></li>
													<li><a href="login.html"><span>Login</span></a></li>
													<li><a href="register.html"><span>Register</span></a></li>
													<li><a href="my-account.html"><span>My account</span></a></li>
                                                    <li><a href="404.html"><span>404</span></a></li>
                                                    <li><a href="404-2.html"><span>404-2</span></a></li>
                                                </ul>
                                            </li>
                                            <li><a href="contact-us.html">Contact</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End mobile menu -->
			</header>
			<!-- End header -->
			<?php echo $content; ?>
<!-- Start top footer -->
			<div class="top-footer">
				<div class="top-content-footer">&nbsp;</div>
			</div>
			<!-- End top footer -->
			<!-- Start ma footer static -->
			<section class="ma-footer-static">
				<div class="footer-static">
					<div class="container">
						<div class="footer-static-top">
							<div class="row">
								<div class="col-sx-12 col-sm-6 col-md-3 f-col-1">
									<a href="#"><img src="img/logo.png" alt=""></a>
									<div class="footer-static-content">
										<p class="des">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>
										<p class="des">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>
									</div>
								</div>
								<div class="col-sx-12 col-sm-6 col-md-3">
									<div class="footer-static-title">
										<h3>Information</h3>
									</div>
									<div class="footer-static-content">
										<ul>
											<li class="first"><a href="#">Sitemap</a></li>
											<li><a href="#">Privacy Policy</a></li>
											<li><a href="#">Your Account</a></li>
											<li><a href="#">Advanced Search</a></li>
											<li class="last"><a href="#">Contact Us</a></li>
										</ul>
									</div>
								</div>
								<div class="col-sx-12 col-sm-6 col-md-3">
									<div class="footer-static-title">
										<h3>Newsletter</h3>
									</div>
									<div class="footer-static-content">
										<div class="block-subscribe">
											<div class="des-newletter">Lorem ipsum dolor sit amet, consectetur adipisicing elit eiusmo delia temporate incididunt labore.</div>
										    <form id="newsletter-validate-detail" method="post" action="https://devitems.com/html/castar-preview/castar/form/subscribe.php">
										        <div class="subscribe-content">
										            <div class="input-box">
										               <input type="text" class="input-text required-entry validate-email" title="Sign up for our newsletter" id="newsletter" placeholder="Enter your e-mail" name="email" name="newsletter">
										            </div>
										            <div class="actions">
										                <button class="button" title="Subscribe" type="submit"><span><span>Subscribe</span></span></button>
										            </div>
										        </div>
										    </form>
										</div>
									</div>
								</div>
								<div class="col-sx-12 col-sm-6 col-md-3 f-col-4">
									<div class="footer-static-title">
										<h3>Contact us</h3>
									</div>
									<div class="footer-static-content">
										<p class="adress"><em class="fa fa-map-marker">&nbsp;</em><span class="ft-content">8901 Marmora Road, Glasgow <span>D04 89 GR, New York</span></span></p>
										<p class="phone"><em class="fa fa-phone">&nbsp;</em><span class="ft-content">Telephone: (801) 2345 - 6789 <span>Fax: (801) 2345 - 6789</span></span></p>
										<p class="email"><em class="fa fa-envelope">&nbsp;</em><span class="ft-content">E-mail: admin@bootexperts.com <span>Website: BootExperts.com</span></span></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End ma footer static -->
			<!-- Start ma footer -->
			<footer class="ma-footer">
				<div class="footer">
					<div class="container">
						<div class="container-inner">
							<div class="row">
								<div class="col-xs-12 col-sm-5">
									<address>Copyright &copy; 2016 <a href="http://bootexperts.com/">BootExperts.</a> All Rights Reserved</address>
								</div>
								<div class="col-xs-12 col-sm-7">
									<ul class="payment">
										<li><a href="#"><img src="img/payment_1.png" alt=""></a></li>
									</ul>	
				    			</div>
							</div>
							<div class="hidden-xs" id="back-top" style="display: block;"></div>
						</div>
					</div>
				</div>
			</footer>
			<!-- End ma footer -->
		</div>
		<!-- End main area -->
		
		
		<!-- jquery-1.12.0.min js
		============================================ -->  
        <script src="<?php echo assets('js/vendor/jquery-1.12.3.min.js'); ?>"></script>
		
		<!-- bootstrap js
		============================================ -->         
        <script src="<?php echo assets('js/bootstrap.min.js'); ?>"></script>
		
		<!-- owl.carousel.min js
		============================================ -->       
        <script src="<?php echo assets('js/owl.carousel.min.js'); ?>"></script>
		
		<!-- jquery.nivo.slider.pack js
        ============================================ -->       
        <script src="<?php echo assets('js/jquery.nivo.slider.pack.js'); ?>"></script>

		<!-- wow js
		============================================ -->       
        <script src="<?php echo assets('js/wow.js'); ?>"></script>
		
        <!-- scrollUp js
        ============================================ -->       
        <script src="<?php echo assets('js/jquery.scrollUp.min.js'); ?>"></script>
	
		<!-- jquery.meanmenu js
        ============================================ -->       
        <script src="<?php echo assets('js/jquery.meanmenu.js'); ?>"></script>

        <!-- plugins js
		============================================ -->         
        <script src="<?php echo assets('js/plugins.js'); ?>"></script>
        
   		<!-- main js
		============================================ -->           
        <script src="<?php echo assets('js/main.js'); ?>"></script>
    </body>

<!-- Mirrored from devitems.com/html/castar-preview/castar/index-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Aug 2016 04:18:11 GMT -->
</html>