<!--- Sidemenu -->
<div id="sidebar-menu">
    <ul>
        <li class="menu-title">Navigation</li>

        <li class=" has_sub">
            <a href="<?php echo base_url('_admin/home'); ?>" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span> Dashboard </span> </a>
        </li>

        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-elevation-rise"></i> <span> Orders </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="<?php echo base_url('_admin/categories/create'); ?>">All Orders</a></li>
                <li><a href="<?php echo base_url('_admin/categories/list'); ?>">Create Orders</a></li>
                <li><a href="<?php echo base_url('_admin/categories/list'); ?>">Pending Orders</a></li>
                <li><a href="<?php echo base_url('_admin/categories/list'); ?>">Cancel Orders</a></li>
            </ul>
        </li>

        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-archive"></i> <span> Brands </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="<?php echo base_url('_admin/brands/create'); ?>">Create Categories</a></li>
                <li><a href="<?php echo base_url('_admin/categories/list'); ?>">List Categories</a></li>
            </ul>
        </li>

        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-archive"></i> <span> Categories </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="<?php echo base_url('_admin/categories/create'); ?>">Create Categories</a></li>
                <li><a href="<?php echo base_url('_admin/categories/list'); ?>">List Categories</a></li>
            </ul>
        </li>

        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-product-hunt"></i> <span> Products </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="<?php echo base_url('_admin/products/create'); ?>">Create Products</a></li>
                <li><a href="<?php echo base_url('_admin/products'); ?>">List Products</a></li>
            </ul>
        </li>

        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bars"></i><span> Reports </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="<?php echo base_url('_admin/reports/year'); ?>">Years Reports</a></li>
                <li><a href="<?php echo base_url('_admin/reports/month'); ?>">Month Reports</a></li>
                <li><a href="<?php echo base_url('_admin/reports/day'); ?>">Day Reports </a></li>
            </ul>
        </li>

        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-support"></i><span> Support Massage </span> </a>
        </li>

        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-users"></i><span> Users Managers </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="<?php echo base_url('_admin/users/create'); ?>">Create Users</a></li>
                <li><a href="<?php echo base_url('_admin/users/list'); ?>">List Users</a></li>
                <li><a href="<?php echo base_url('_admin/users/locked'); ?>">Locked Users</a></li>
            </ul>
        </li>

        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cogs"></i><span> Website Settings </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="<?php echo base_url('_admin/settings/create'); ?>">Website Settings</a></li>
                <li><a href="<?php echo base_url('_admin/settings/list'); ?>">Cloudflare Settings</a></li>
                <li><a href="<?php echo base_url('_admin/settings/locked'); ?>">API Management</a></li>
            </ul>
        </li>

    </ul>
</div>
<!-- Sidebar -->
<div class="clearfix"></div>
