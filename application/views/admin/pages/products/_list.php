<?php if (!empty($products)): ?>
    <table id="datatable" class="table table-striped table-bordered datatable">
        <thead>
            <tr>
                <th>Photo</th>
                <th>Title</th>
                <th>Brand</th>
                <th>Category</th>
                <th>Price</th>
                <th>Stock</th>
                <th>Publish</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>

            <?php foreach ($products as $product): ?>
            <tr>
                <td class="text-center">
                    <img src="<?php echo base_url('media/thumbs/'.$this->Productsimage->selectByKey($product->image_key, 1, 'filename')); ?>">
                </td>
                <td>
                    <?php echo xssclear($product->name); ?>
                </td>
                <td>
                    <strong><?php echo xssclear($this->Brand->select_name($product->brands_id)); ?></strong>
                </td>
                <td>
                    <strong><?php echo($this->ProductsHaveCategories->select_products_list($product->id)); ?></strong>
                </td>
                <td>
                    <?php echo rupiah($product->price); ?>
                </td>
                <td>
                    <?php echo xssclear($product->stock); ?>
                </td>
                 <td>
                    <?php if ($product->publish=="Y"): ?>
                        <span class="label label-success">Publish</span>
                    <?php else: ?>
                        <span class="label label-danger">Draft</span>
                    <?php endif; ?>
                </td>
                <td>
                    <a href="<?php echo base_url('_admin/products/'.xssclear($product->id).'/edit'); ?>"><button type="button" class="btn btn-info"> <i class="fa fa-pencil"></i> </button></a>
                    <button id="deletebutton" type="button" class="btn btn-danger deletebutton" data-product-id="<?php echo xssclear($product->id); ?>"> <i class="fa fa-trash"></i> </button></a>
                </td>
            </tr>
            <?php endforeach; ?>

        <tbody>
    </table>
<?php else: ?>
    Maaf Tidak Ditemukan Data Products <?php if (isset($search)): ?>
        dengan nama "<?php echo $search ?>"
    <?php endif; ?>
<?php endif; ?>
