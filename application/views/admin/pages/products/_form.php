
<?php if (!empty(validation_errors())): ?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo validation_errors(); ?>
    </div>
<?php endif ?>
<?php flashmassage() ?>

<div class="form-group clearfix">
    <div class="col-sm-12 padding-left-0 padding-right-0">
            <input type="file" name="product_photos" id="filer_input1"
               multiple>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Nama Product</label>
    <div class="col-md-10">
        <input class="form-control" name="nama-product" type="text" data-parsley-minlength="6" data-parsley-maxlength="100" placeholder="Masukan Nama Product Anda" <?php if (isset($product)): ?>
                value="<?php echo xssclear($product->name); ?>"
            <?php endif ?> required>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Brand</label>
    <div class="col-md-8">
        <?php if (!is_null($brands)): ?>
            <select name="brand" class="form-control" required>
                <option> == Pilih Brand Product ==</option>
                <?php foreach ($brands as $brand): ?>
                    <option value="<?php echo xssclear($brand->id); ?>" <?php if (isset($product) && $brand->id==$product->brands_id): ?>
                        selected
                    <?php endif ?>> <?php echo xssclear($brand->name); ?> </option>
                <?php endforeach; ?>
            </select>

        <?php else: ?>
            <p>Maaf, Anda harus Membuat <a href="<?php echo base_url('_admin/brands/create'); ?>"> Brands Products </a> Terlebih dahulu.</p>
        <?php endif; ?>
    </div>
    <div class="col-md-2">
        <a href="<?php echo base_url('_admin/brands/create'); ?>"><button type="button" class="btn btn-info" style="width:100%;"> <i class="fa fa-pencil"></i> Create Brands </button></a>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Catagories</label>
    <div class="col-md-8">
        <?php if (!is_null($categories)): ?>
            <select name="category[]" class="form-control" multiple required>
                <?php foreach ($categories as $category): ?>
                    <option value="<?php echo xssclear($category->id); ?>" <?php if (isset($product) && $this->ProductsHaveCategories->matched($product->id, $category->id)==true): ?>
                        selected
                    <?php endif ?>> <?php echo xssclear($category->name); ?> </option>
                <?php endforeach; ?>
            </select>
        <?php else: ?>
            <p>Maaf, Anda harus Membuat <a href="<?php echo base_url('_admin/categories/create'); ?>"> Category </a> Terlebih dahulu.</p>
        <?php endif; ?>
    </div>
    <div class="col-md-2">
        <a href="<?php echo base_url('_admin/categories/create'); ?>"><button type="button" class="btn btn-warning" style="width:100%;"> <i class="fa fa-pencil"></i> Create Category </button></a>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Feature Product</label>
    <div class="col-md-10">
        <textarea class="form-control" name="feature" data-parsley-maxlength="450" placeholder="Feature - Feature Products" required><?php if (isset($product)): ?><?php echo $product->feature; ?><?php endif ?></textarea>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Deskripsi Product</label>
    <div class="col-md-10">
        <textarea class="form-control texteditor" name="deskripsi" placeholder="Deskripsi products lebih rinci">
            <?php if (isset($product)): ?><?php echo($product->detail); ?>
            <?php endif ?>
        </textarea>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Harga Products</label>
    <div class="col-md-10">
        <input type="text" name="price" placeholder="Masukan Harga Product" data-v-max="9999999999999" data-v-min="0" data-a-sign="Rp. " <?php if (isset($product)): ?>
                value="<?php echo xssclear($product->price); ?>"
            <?php endif ?> class="form-control autonumber" required>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Berat Products</label>
    <div class="col-md-10">
        <input type="text" name="weight" placeholder="Masukan Berat Product" data-v-max="99999999999" data-a-sign=" Kg" data-v-min="0" data-p-sign="s" <?php if (isset($product)): ?>
                value="<?php echo xssclear($product->weight); ?>"
            <?php endif ?>  class="form-control autonumber"required>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Stock Products</label>
    <div class="col-md-10">
        <input type="text" name="stock" placeholder="Masukan Berat Product" data-v-max="9999999999"  data-a-sign=" Barang" data-v-min="0" data-p-sign="s" <?php if (isset($product)): ?>
                value="<?php echo xssclear($product->stock); ?>"
            <?php endif ?>  class="form-control autonumber"required>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Publish Products</label>
    <div class="col-md-10">
        <input type="checkbox" name="publish" value="Y" <?php if (isset($product) && $product->publish=="Y"): ?>
            checked
        <?php endif ?> data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
    </div>
</div>

<div class="form-group text-center">
    <!-- <button class="btn btn-info" type="reset">Reset</button> -->
    <button class="btn btn-primary" type="submit">Submit</button>
</div>