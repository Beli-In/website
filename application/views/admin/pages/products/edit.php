<div class="container">


    <div class="row">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Edit Products "<?php echo $product->name; ?>" </h4>
                <ol class="breadcrumb p-0 m-0">
                    <li>
                        <a href="<?php echo base_url('_admin'); ?>">Admin Area</a>
                    </li>
                    <li>
                        <a href="#">Products </a>
                    </li>
                    <li class="active">
                        Edit
                    </li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Edit Products "<?php echo $product->name; ?>"</b></h4>
                <hr/>
                <?php echo form_open(current_url(), ['class' => 'form-horizontal', 'id' => 'create-products']); ?>
                    <?php echo $this->load->view('admin/pages/products/_form', false, true); ?>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <!-- end row -->



</div> <!-- container -->
