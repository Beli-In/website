<div class="container">


    <div class="row">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">List Products </h4>
                <ol class="breadcrumb p-0 m-0">
                    <li>
                        <a href="<?php echo base_url('_admin'); ?>">Admin Area</a>
                    </li>
                    <li>
                        <a href="#">Products </a>
                    </li>
                    <li class="active">
                        List Products
                    </li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b> Mencari Product "<?php echo $search ?>"</h4>
                <hr/>
                <a href="<?php echo base_url('_admin/brands/create'); ?>"><button type="button" class="btn btn-info"> <i class="fa fa-pencil"></i> Create Brands </button></a>
                <a href="<?php echo base_url('_admin/categories/create'); ?>"><button type="button" class="btn btn-warning"> <i class="fa fa-pencil"></i> Create Category </button></a>
                <a href="<?php echo base_url('_admin/products/create'); ?>"><button type="button" class="btn btn-success"> <i class="fa fa-pencil"></i> Create Products </button></a>
                <div class="pull-right">
                        <?php echo form_open(current_url(), ['class' => 'form-inline', 'method' => 'GET']); ?>
                            <input class="form-control" name="product" type="text" placeholder="Search Products" value="<?php echo $search; ?>">
                            <input type="submit" class="btn btn-default" value="search">
                        <?php echo form_close(); ?>
                </div>
                <div class="clearfix" style="padding-top:25px;"></div>
                <?php flashmassage() ?>
                <div class="clearfix" style="padding-top:25px;"></div>
                <?php echo $this->load->view('admin/pages/products/_list', false, true); ?>
                <div class="text-center">
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->



</div> <!-- container -->
