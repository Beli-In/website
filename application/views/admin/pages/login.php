<!DOCTYPE html>
<html>

<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- App title -->
        <title>
            <?php
                    echo 'Login Admin Area || ' . $this->Options->get('name');
            ?>
        </title>

        <!-- App css -->
        <link href="<?php echo privassets('css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo privassets('css/core.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo privassets('css/components.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo privassets('css/icons.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo privassets('css/pages.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo privassets('css/menu.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo privassets('css/responsive.css'); ?>" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo privassets('js/modernizr.min.js'); ?>"></script>

    </head>


    <body class="bg-transparent">

        <!-- HOME -->
        <section>
            <div class="container-alt">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="wrapper-page">

                            <div class="m-t-40 account-pages">
                                <div class="text-center account-logo-box">
                                    <h2 class="text-uppercase">
                                        <a href="index.html" class="text-success">
                                            <span><img src="<?php echo assets($this->Options->get('logo')); ?>" alt="" height="36"></span>
                                        </a>
                                    </h2>
                                    <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                                </div>
                                <div class="account-content">
                                    <?php echo form_open(current_url(), ['class' => 'form-horizontal']) ?>
                                        <div>
                                            <?php if (!empty(validation_errors())): ?>
                                                <div class="alert alert-danger">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    <?php echo validation_errors(); ?>
                                                </div>
                                            <?php endif ?>
                                            <?php flashmassage() ?>
                                        </div>
                                        <div class="form-group ">
                                            <div class="col-xs-12">
                                                <input class="form-control" name="email" type="email" required="" placeholder="E-mail">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <input class="form-control" name="password" type="password" required="" placeholder="Password">
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <div class="col-xs-12">
                                                <div class="checkbox checkbox-success">
                                                    <input id="checkbox-signup" type="checkbox" name="rememberme">
                                                    <label for="checkbox-signup">
                                                        Remember me
                                                    </label>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group text-center">
                                            <div class="col-xs-12 text-center">
                                                <?php echo recaptcha_open(); ?>
                                            </div>
                                        </div>

                                        <div class="form-group account-btn text-center m-t-10">
                                            <div class="col-xs-12">
                                                <button class="btn w-md btn-bordered btn-danger waves-effect waves-light" type="submit">Log In</button>
                                            </div>
                                        </div>

                                    <?php echo form_close(); ?>

                                    <div class="clearfix"></div>

                                </div>
                            </div>
                            <!-- end card-box-->

                        </div>
                        <!-- end wrapper -->

                    </div>
                </div>
            </div>
          </section>
          <!-- END HOME -->

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo privassets('js/jquery.min.js'); ?>"></script>
        <script src="<?php echo privassets('js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo privassets('js/detect.js'); ?>"></script>
        <script src="<?php echo privassets('js/fastclick.js'); ?>"></script>
        <script src="<?php echo privassets('js/jquery.blockUI.js'); ?>"></script>
        <script src="<?php echo privassets('js/waves.js'); ?>"></script>
        <script src="<?php echo privassets('js/jquery.slimscroll.js'); ?>"></script>
        <script src="<?php echo privassets('js/jquery.scrollTo.min.js'); ?>"></script>

        <!-- App js -->
        <script src="<?php echo privassets('js/jquery.core.js'); ?>"></script>
        <script src="<?php echo privassets('js/jquery.app.js'); ?>"></script>

    </body>

<!-- Mirrored from coderthemes.com/zircos_1.4/default/page-login.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 16 Sep 2016 14:44:46 GMT -->
</html>
