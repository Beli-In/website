<!-- Start breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="container-inner">
			<ul>
				<li class="home">
					<a title="Go to Home Page" href="#">Home</a>
					<span><i class="fa fa-angle-double-right"></i></span>
				</li>
				<li class="product"><strong>Blog</strong></li>
			</ul>
		</div>
	</div>
</div>
<!-- End breadcrumbs -->
<!-- Start page content -->
<section class="blog-area">
     <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="bolg-side-bar">
                        <div class="blog-categoris">
                            <h3>Blog Categories</h3>
                            <ul class="bside-menu">
                                <li><a href="#">News</a></li>
                                <li><a href="#">About Beauty</a></li>
                                <li><a href="#">Baby &amp; Mom</a></li>
                                <li><a href="#">Diet &amp; Fitness</a></li>
                                <li><a href="#">Promotions</a></li>
                            </ul>
                        </div>
                        <div class="section-offset">
                            <h3>Popular Posts</h3>
                            <ul class="list-of-entries">
                                <li>     
                                    <div class="entry">
                                        <a href="#" class="entry-thumb">
                                            <img src="<?php echo assets('img/blog/popular-post1.jpg'); ?>" alt="">
                                        </a>
                                        <div class="wrapper">
                                            <h6 class="entry-title"><a href="single-blog.html">Lorem ipsum dolor sit amet, Neque porro quis- quam</a></h6>
                                            <div class="entry-meta">
                                                <span><i class="fa fa-calendar"></i> 2015-11-05</span>
                                                <span><a href="#" class="comments"><i class="fa fa-comment"></i> 3</a></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>   
                                    <div class="entry">
                                        <a href="#" class="entry-thumb">
                                            <img src="<?php echo assets('img/blog/popular-post2.jpg'); ?>" alt="">
                                        </a>
                                        <div class="wrapper">
                                            <h6 class="entry-title"><a href="single-blog.html">Neque porro quis- quam Lorem ipsum dolor sit amet.</a></h6>
                                            <div class="entry-meta">
                                                <span><i class="fa fa-calendar"></i> 2015-11-15</span>
                                                <span><a href="#" class="comments"><i class="fa fa-comment"></i> 3</a></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>     
                                    <div class="entry">
                                        <a href="#" class="entry-thumb">
                                            <img src="<?php echo assets('img/blog/popular-post3.jpg'); ?>" alt="">
                                        </a>
                                        <div class="wrapper">
                                            <h6 class="entry-title"><a href="single-blog.html">Mauris quis- quam Lorem ipsum dolor fermentum dictum</a></h6>
                                            <div class="entry-meta">
                                                <span><i class="fa fa-calendar"></i> 2015-12-05</span>
                                                <span><a href="#" class="comments"><i class="fa fa-comment"></i> 3</a></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="section-offset2">
                            <h3>Recent Comments</h3>
                            <ul class="recentcomments-list">
                                <li class="recentcomment">
                                    <div class="posted-by">Posted by: <a href="#">admin</a>:</div>
                                    <a href="#">Lorem ipsum dolor sit amet, con- sectetuer adipis</a>
                                    <p class="comment">Fusce euismod consequat ante. Lorem ipsum dolor sit amet...</p>
                                </li>
                                <li class="recentcomment">
                                    <div class="posted-by">Posted by: <a href="#">admin</a>:</div>
                                    <a href="#">Neque porro quisquam est, qui</a>
                                    <p class="comment">Consectetuer adipis. Mauris accumsan nulla vel diam. Sed in...</p>
                                </li>
                                <li class="recentcomment">
                                    <div class="posted-by">Posted by: <a href="#">admin</a>:</div>
                                    <a href="#">Mauris fermentum dictum magna</a>
                                    <p class="comment">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices...</p>
                                </li>
                            </ul>
                        </div>
                        <div class="section-offset3">
                            <h3>Blog Tags</h3>
                            <div class="tags_container">
                                <ul class="tags-cloud">
                                    <li><a href="#" class="button_grey">allergy</a></li>
                                    <li><a href="#" class="button_grey">baby</a></li>
                                    <li><a href="#" class="button_grey">beauty</a></li>
                                    <li><a href="#" class="button_grey">ear care</a></li>
                                    <li><a href="#" class="button_grey">for her</a></li>
                                    <li><a href="#" class="button_grey">for him</a></li>
                                    <li><a href="#" class="button_grey">first aid</a></li>
                                    <li><a href="#" class="button_grey">gift sets</a></li>
                                    <li><a href="#" class="button_grey">spa</a></li>
                                    <li><a href="#" class="button_grey">hair care</a></li>
                                    <li><a href="#" class="button_grey">herbs</a></li>
                                    <li><a href="#" class="button_grey">medicine</a></li>
                                    <li><a href="#" class="button_grey">natural</a></li>
                                    <li><a href="#" class="button_grey">oral care</a></li>
                                    <li><a href="#" class="button_grey">pain</a></li>
                                    <li><a href="#" class="button_grey">pedicure</a></li>
                                    <li><a href="#" class="button_grey">personal care</a></li>
                                    <li><a href="#" class="button_grey">probiotics</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Start blog post -->
                <div class="col-xs-12 col-sm-8 col-md-8">
                    <div class="blog-post2">
                        <div class="blog-box2">
                            <div class="entry-date">
                                <div class="day">20</div>
                                <div class="month">Jun</div>
                            </div>  
                            <div class="entry-main-content">
                                <div class="entry-thumbnail">
                                    <img src="<?php echo assets('img/blog/blog-1.jpg'); ?>" alt="">
                                    <div class="block_hover">
                                        <div class="blog-link">
                                            <a href="<?php echo assets('img/blog/blog-1.jpg'); ?>" class="fancybox"><i data-fancybox-group="gallery" class="fa fa-search"></i></a> 
                                            <a href="single-blog.html"><i class="fa fa-link"></i></a>
                                        </div>
                                    </div>
                                </div>                 
                                <div class="entry-content-other">
                                    <h3><a href="#">Ut tellus dolor, dapibus eget,</a></h3>
                                    <p><a href="single-blog.html"><i class="fa fa-user"></i> Admin</a><a href="#"><i class="fa fa-comment"></i> Comment 03</a></p>
                                    <div class="summary">
                                        <p>Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consecvtetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus. Vestibulum libero nisl, porta vel, scelerisque eget, malesuada at, neque</p>    
                                        <a class="read-more-link" href="single-blog.html">Read More</a> 
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End blog post -->
                    <!-- Start blog post -->
                    <div class="blog-post2">
                        <div class="blog-box2">
                            <div class="entry-date">
                                <div class="day">22</div>
                                <div class="month">Jun</div>
                            </div>  
                            <div class="entry-main-content">
                                <div class="entry-thumbnail">
                                    <img src="<?php echo assets('img/blog/blog-2.jpg'); ?>" alt="">
                                    <div class="block_hover">
                                        <div class="blog-link">
                                            <a href="<?php echo assets('img/blog/blog-2.jpg'); ?>" class="fancybox"><i data-fancybox-group="gallery" class="fa fa-search"></i></a> 
                                            <a href="single-blog.html"><i class="fa fa-link"></i></a>
                                        </div>
                                    </div>
                                </div>                 
                                <div class="entry-content-other">
                                    <h3><a href="#">elementum vel, cursus eleifend,</a></h3>
                                    <p><a href="single-blog.html"><i class="fa fa-user"></i> Admin</a><a href="#"><i class="fa fa-comment"></i> Comment 10</a></p>
                                    <div class="summary">
                                        <p>Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consecvtetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus. Vestibulum libero nisl, porta vel, scelerisque eget, malesuada at, neque</p>    
                                        <a class="read-more-link" href="single-blog.html">Read More</a> 
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End blog post -->
                    <!-- Start blog post -->
                    <div class="blog-post2">
                        <div class="blog-box2">
                            <div class="entry-date">
                                <div class="day">28</div>
                                <div class="month">Jun</div>
                            </div>  
                            <div class="entry-main-content">
                                <div class="entry-thumbnail">
                                    <img src="<?php echo assets('img/blog/blog-3.jpg'); ?>" alt="">
                                    <div class="block_hover">
                                        <div class="blog-link">
                                            <a href="<?php echo assets('img/blog/blog-3.jpg'); ?>" class="fancybox"><i data-fancybox-group="gallery" class="fa fa-search"></i></a> 
                                            <a href="single-blog.html"><i class="fa fa-link"></i></a>
                                        </div>
                                    </div>
                                </div>                 
                                <div class="entry-content-other">
                                    <h3><a href="#"> Aenean auctor wisi et urna.</a></h3>
                                    <p><a href="single-blog.html"><i class="fa fa-user"></i> Admin</a><a href="#"><i class="fa fa-comment"></i> Comment 22</a></p>
                                    <div class="summary">
                                        <p>Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consecvtetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus. Vestibulum libero nisl, porta vel, scelerisque eget, malesuada at, neque</p>    
                                        <a class="read-more-link" href="single-blog.html">Read More</a> 
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End blog post -->
                    <!-- Start blog post -->
                    <div class="blog-post2">
                        <div class="blog-box2">
                            <div class="entry-date">
                                <div class="day">12</div>
                                <div class="month">Jun</div>
                            </div>  
                            <div class="entry-main-content">
                                <div class="entry-thumbnail">
                                    <img src="<?php echo assets('img/blog/blog-4.jpg'); ?>" alt="">
                                    <div class="block_hover">
                                        <div class="blog-link">
                                            <a href="<?php echo assets('img/blog/blog-4.jpg'); ?>" class="fancybox"><i data-fancybox-group="gallery" class="fa fa-search"></i></a> 
                                            <a href="single-blog.html"><i class="fa fa-link"></i></a>
                                        </div>
                                    </div>
                                </div>                 
                                <div class="entry-content-other">
                                    <h3><a href="#">Aliquam erat volutpat.</a></h3>
                                    <p><a href="single-blog.html"><i class="fa fa-user"></i> Admin</a><a href="#"><i class="fa fa-comment"></i> Comment 07</a></p>
                                    <div class="summary">
                                        <p>Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consecvtetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus. Vestibulum libero nisl, porta vel, scelerisque eget, malesuada at, neque</p>    
                                        <a class="read-more-link" href="single-blog.html">Read More</a> 
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End blog post -->
                    <!-- Start blog post -->
                    <div class="blog-post2">
                        <div class="blog-box2">
                            <div class="entry-date">
                                <div class="day">18</div>
                                <div class="month">Jun</div>
                            </div>  
                            <div class="entry-main-content">
                                <div class="entry-thumbnail">
                                    <img src="<?php echo assets('img/blog/blog-5.jpg'); ?>" alt="">
                                    <div class="block_hover">
                                        <div class="blog-link">
                                            <a href="<?php echo assets('img/blog/blog-5.jpg'); ?>" class="fancybox"><i data-fancybox-group="gallery" class="fa fa-search"></i></a> 
                                            <a href="single-blog.html"><i class="fa fa-link"></i></a>
                                        </div>
                                    </div>
                                </div>                 
                                <div class="entry-content-other">
                                    <h3><a href="#">Duis ac turpis. Donec sit amet eros.</a></h3>
                                    <p><a href="single-blog.html"><i class="fa fa-user"></i> Admin</a><a href="#"><i class="fa fa-comment"></i> Comment 08</a></p>
                                    <div class="summary">
                                        <p>Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consecvtetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus. Vestibulum libero nisl, porta vel, scelerisque eget, malesuada at, neque</p>    
                                        <a class="read-more-link" href="single-blog.html">Read More</a> 
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End blog post -->
                    <!-- Start blog post -->
                    <div class="blog-post2">
                        <div class="blog-box2">
                            <div class="entry-date">
                                <div class="day">05</div>
                                <div class="month">Jun</div>
                            </div>  
                            <div class="entry-main-content">
                                <div class="entry-thumbnail">
                                    <img src="<?php echo assets('img/blog/blog-6.jpg'); ?>" alt="">
                                    <div class="block_hover">
                                        <div class="blog-link">
                                            <a href="<?php echo assets('img/blog/blog-6.jpg'); ?>" class="fancybox"><i data-fancybox-group="gallery" class="fa fa-search"></i></a> 
                                            <a href="single-blog.html"><i class="fa fa-link"></i></a>
                                        </div>
                                    </div>
                                </div>                 
                                <div class="entry-content-other">
                                    <h3><a href="#">Mauris fermentum dictum magna.</a></h3>
                                    <p><a href="single-blog.html"><i class="fa fa-user"></i> Admin</a><a href="#"><i class="fa fa-comment"></i> Comment 5</a></p>
                                    <div class="summary">
                                        <p>Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consecvtetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus. Vestibulum libero nisl, porta vel, scelerisque eget, malesuada at, neque</p>    
                                        <a class="read-more-link" href="single-blog.html">Read More</a> 
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End blog post -->
                    <!-- Start blog post -->
                    <div class="blog-post2">
                        <div class="blog-box2">
                            <div class="entry-date">
                                <div class="day">15</div>
                                <div class="month">Jun</div>
                            </div>  
                            <div class="entry-main-content">
                                <div class="entry-thumbnail">
                                    <img src="<?php echo assets('img/blog/blog-7.jpg'); ?>" alt="">
                                    <div class="block_hover">
                                        <div class="blog-link">
                                            <a href="<?php echo assets('img/blog/blog-7.jpg'); ?>" class="fancybox"><i data-fancybox-group="gallery" class="fa fa-search"></i></a> 
                                            <a href="single-blog.html"><i class="fa fa-link"></i></a>
                                        </div>
                                    </div>
                                </div>                 
                                <div class="entry-content-other">
                                    <h3><a href="#">Sed laoreet aliquam leo.</a></h3>
                                    <p><a href="single-blog.html"><i class="fa fa-user"></i> Admin</a><a href="#"><i class="fa fa-comment"></i> Comment 10</a></p>
                                    <div class="summary">
                                        <p>Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consecvtetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus. Vestibulum libero nisl, porta vel, scelerisque eget, malesuada at, neque</p>    
                                        <a class="read-more-link" href="single-blog.html">Read More</a> 
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End blog post -->
                    <!-- Start blog pagination -->
                    <div class="blog-pagination">
                        <ul class="pagination">
                            <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        </ul>
                    </div>
                    <!-- End blog pagination -->
                </div>    
            </div>
        </div>
</section>