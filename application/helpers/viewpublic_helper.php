<?php
defined('BASEPATH') or die("You Don't Have Authorization To Access this File");

if (!function_exists('assets')) {
    function assets($url)
    {
        return base_url('public/'.$url);
    }
}

if (!function_exists('privassets')) {
    function privassets($url)
    {
        return base_url('private/'.$url);
    }
}

if (!function_exists('recaptcha_open')) {
    function recaptcha_open()
    {
        $CI =& get_instance();
        return '<div class="g-recaptcha" data-sitekey="'.$CI->config->item('re-publickey').'"></div><br/>
		<script src="https://www.google.com/recaptcha/api.js"></script>';
    }
}

if (!function_exists('view')) {
    function view($pathView, $var = null, $baselayout = 'layouts/app')
    {
        $CI =& get_instance();
        $var['content'] = $CI->load->view($pathView, $var, true);
        $CI->load->view($baselayout, $var);
    }
}

if (!function_exists('adview')) {
    function adview($pathView, $var = null, $baselayout = 'layouts/admin')
    {
        $CI =& get_instance();
        $CI->load->library('ion_auth');
        $var['user'] = $CI->ion_auth->user()->row();
        // print_r($var['user']);
        if ($baselayout==null) {
            $CI->load->view($pathView, $var);
        } else {
            $var['content'] = $CI->load->view($pathView, $var, true);
            $CI->load->view($baselayout, $var);
        }
    }
}

if (!function_exists('flash')) {
    function flash($data, $type = 'danger')
    {
        $CI =& get_instance();
        $array = array(
            'alert_notif' => $data,
            'alert_type' => $type
        );

        $CI->session->set_userdata($array);
    }
}

if (!function_exists('getFlash')) {
    function getFlash($data, $last = false)
    {
        $CI =& get_instance();
        if (!empty($CI->session->userdata('alert_notif'))) {
            $array = [
                'notif' => $CI->session->userdata('alert_notif'),
                'type' => $CI->session->userdata('alert_type')
            ];
            if ($last==true) {
                $CI->session->unset_userdata('alert_notif');
                $CI->session->unset_userdata('alert_type');
            }
            return $array[$data];
        }
        return false;
    }
}

if (!function_exists('flashmassage')) {
    function flashmassage()
    {
        if (getFlash("notif")!==false) {
            echo '
            <div class="alert alert-'.getFlash('type').'">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					'.getFlash('notif', true).'
            </div>';
        }
    }
}

if (!function_exists('admenu')) {
    function admenu($name)
    {
        $CI =& get_instance();
        if ($CI->uri->segment(2)==$name) {
            return 'active';
        }
    }
}

if (!function_exists('users')) {
    function users($fields)
    {
        $CI =& get_instance();
        $CI->load->library('ion_auth');
        $users = $CI->ion_auth->user();
        return $users->{$fields};
    }
}

if (!function_exists('rupiah')) {
    function rupiah($uang)
    {
        return "Rp. ".number_format($uang, 0, '', ',');
    }
}

if (!function_exists('xssclear')) {
    function xssclear($str)
    {
        return htmlentities(trim(preg_replace('/(<|>|"|\'|~|!|@|\$|%|\^|&|\*|\(|\)|\+|`|;|\?|,|=|\|)|/', '', strip_tags(stripslashes($str)))), ENT_NOQUOTES, "UTF-8");
    }
}
